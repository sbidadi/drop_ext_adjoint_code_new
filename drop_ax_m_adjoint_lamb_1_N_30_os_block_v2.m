function [capl,grad] = drop_ax_m_adjoint_lamb_1_N_30_os_block_v2 ...
    (NSG,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
    Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s)

format long g
NGL = 6;

inv_capl = capl^(-1.0);
hf = 0.5;

hsize = NSG/2;

% ************* computing diff between current and ref velocity, and arc length between two marker points ******************
for j=1:NSG+1

  % computing diff between current and ref velocity
  Dfx(j) = (velx(j) - velx_ref(j));
  Dfy(j) = (vely(j) - vely_ref(j));

  % computing the arc length between two marker points
  if (j == 1)
    ds(j) = 2.0*s(j+1);

  else
    ds(j) = s(j+1) - s(j-1);

   end
end

% *********** computing the off-surface position and velocities *************
[X0_vec_os,Y0_vec_os,velx_os,vely_os] = drop_ax_m_adjoint_lamb_1_u_pm_v2 ...            
(NSG,NGL,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
                Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s);

% *********** computing the off-surface pressure *************
[pr_vec_os,prc_vec_os, X0c_vec_os, Y0c_vec_os, qx, qy] = drop_ax_m_adjoint_lamb_1_p_pm_v2 ...
               (NSG,NGL,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
                Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s);

% ***************** Computing the necessary gradients ****************

% 1. computing n dvel/dn and dp/dn *******************

for i=1:NSG+1

    velx_1 = velx_os(i,1);
    vely_1 = vely_os(i,1);
    X0_1 = X0_vec_os(i,1);
    Y0_1 = Y0_vec_os(i,1);
    X0c_1 = X0c_vec_os(i,1);
    Y0c_1 = Y0c_vec_os(i,1);
    p_1 = pr_vec_os(i,1);
    pc_1 = prc_vec_os(i,1);   

    velx_2 = velx_os(i,2);
    vely_2 = vely_os(i,2);
    X0_2 = X0_vec_os(i,2);
    Y0_2 = Y0_vec_os(i,2);
    X0c_2 = X0c_vec_os(i,2);
    Y0c_2 = Y0c_vec_os(i,2);
    p_2 = pr_vec_os(i,2);
    pc_2 = prc_vec_os(i,2);
 
    velx_3 = velx_os(i,3);
    vely_3 = vely_os(i,3);
    X0_3 = X0_vec_os(i,3);
    Y0_3 = Y0_vec_os(i,3);
    X0c_3 = X0c_vec_os(i,3);
    Y0c_3 = Y0c_vec_os(i,3);
    p_3 = pr_vec_os(i,3);
    pc_3 = prc_vec_os(i,3);
 
    velx_4 = velx_os(i,4);
    vely_4 = vely_os(i,4);
    X0_4 = X0_vec_os(i,4);
    Y0_4 = Y0_vec_os(i,4);
    X0c_4 = X0c_vec_os(i,4);
    Y0c_4 = Y0c_vec_os(i,4);
    p_4 = pr_vec_os(i,4);
    pc_4 = prc_vec_os(i,4);

  % computing n dvel/dn
  velxx(i) = (velx_1 - velx_3)/(X0_1 - X0_3);
  velxy(i) = (velx_2 - velx_4)/(Y0_2 - Y0_4);
  velyx(i) = (vely_1 - vely_3)/(X0_1 - X0_3);
  velyy(i) = (vely_2 - vely_4)/(Y0_2 - Y0_4);

%  if (i >= hsize && i <= hsize+2)
%        fprintf('%20.14f\t%20.14f\t%20.14f\t%20.14f%20.14f\n',vely_2,vely_4,Y0_2,Y0_4,YI(i));
%  end

  du_dn(i) = velxx(i)*vnx(i) + velxy(i)*vny(i);
  dv_dn(i) = velyx(i)*vnx(i) + velyy(i)*vny(i);

  n_du_n(i) = du_dn(i)*vnx(i) + dv_dn(i)*vny(i);

  % computing dp/dn
  dp_dx(i) = (p_1 - p_3)/(X0_1 - X0_3);
  dp_dy(i) = (p_2 - p_4)/(Y0_2 - Y0_4);
  dp_dn(i) = dp_dx(i)*vnx(i) + dp_dy(i)*vny(i);


%  dp_dx_p(i) = (p_1 - pc_1)/(X0_1 - X0c_1);
%  dp_dy_p(i) = (p_2 - pc_2)/(Y0_2 - Y0c_2);
%
%  dp_dx_m(i) = (p_3 - pc_3)/(X0_3 - X0c_3);
%  dp_dy_m(i) = (p_4 - pc_4)/(Y0_4 - Y0c_4);
%
%  dpp_dn(i) = dp_dx_p(i)*vnx(i) + dp_dy_p(i)*vny(i);
%  dpm_dn(i) = dp_dx_m(i)*vnx(i) + dp_dy_m(i)*vny(i);
%
%  dp_dn(i) = dpp_dn(i) - dpm_dn(i);
%
end

%du_dn = smooth(NSG,du_dn,1);
%dv_dn = smooth(NSG,dv_dn,1);
%n_du_n = smooth(NSG,n_du_n,1);
%dp_dn = smooth(NSG,dp_dn,1);
%

X0_vec_os
X0c_vec_os

%velx_os
%vely_os
pr_vec_os
prc_vec_os

du_dn'
dv_dn'
n_du_n'

%dp_dx_p'
%dp_dy_p'

Y0_vec_os(:,2)
Y0c_vec_os(:,2)

%dpp_dn'
%dpm_dn'
%dp_dn'

figure(21)
plot(XI,velxx,'o',XI,velxy,'--o')
xlabel('XI')
legend('velxx','velxy')

figure(22)
plot(XI,velyx,'o',XI,velyy,'--o')
xlabel('XI')
legend('velyx','velyy')

figure(20)
plot(XI,du_dn,'o',XI,dv_dn,'--o')
xlabel('XI')
legend('du/dn','dv_dn')

figure(30)
plot(XI,dp_dn,'o')
xlabel('XI')
legend('dp/dn')

% 2. computing dcrvm stuff *******************
for i = 1:NSG+1

    if (i == 1)

     dcrvm_vnx(i) = (crvm(i+1)-crvm(i))/(vnx(i+1)-vnx(i));
     dcrvm_vny(i) = 0.0;

     dvnx_ds(i) = 0.0;
     dvny_ds(i) = vny(i+1)/s(i+1);

    elseif (i == NSG+1)

     dcrvm_vnx(i) = (crvm(i)-crvm(i-1))/(vnx(i)-vnx(i-1));
     dcrvm_vny(i) = 0.0;

     dvnx_ds(i) = 0.0;
     dvny_ds(i) = vny(NSG)/s(NSG);

    else

     dcrvm_vnx(i) = (crvm(i+1)-crvm(i-1))/(vnx(i+1)-vnx(i-1));
     dcrvm_vny(i) = (crvm(i+1)-crvm(i-1))/(vny(i+1)-vny(i-1));

     dvnx_ds(i) = (vnx(i+1)-vnx(i-1))/(s(i+1)-s(i-1));
     dvny_ds(i) = (vny(i+1)-vny(i-1))/(s(i+1)-s(i-1));

    end

end

%dcrvm_vnx = smooth(NSG,dcrvm_vnx,1);
%dcrvm_vny = smooth(NSG,dcrvm_vny,1);
%dvnx_ds = smooth(NSG,dvnx_ds,1);
%dvny_ds = smooth(NSG,dvny_ds,1);
%
%velx_nx
%vely_ny
%dcrvm_vnx(1) = dcrvm_vnx(2);
%dcrvm_vny(1) = dcrvm_vny(2);
%dcrvm_vnx(NSG+1) = dcrvm_vnx(NSG);
%dcrvm_vny(NSG+1) = dcrvm_vny(NSG);
%dcrvm_vnx'
%dcrvm_vny'

% **************** Computing coefficients alpha, beta, gamma, tau_1x, tau_1y, psi_x, psi_y *****************
[alpha,beta,gamma_x,gamma_y,tau_1x,tau_1y,tau_2x,tau_2y,psi_x,psi_y,tau] = drop_ax_adjoint_coefficients ... 
(inv_capl,velx,vely,vnx,vny,vtx,vty,s,crvm,dvnx_ds,dvny_ds,dp_dn,n_du_n,dcrvm_vnx,dcrvm_vny,NSG);

%[alpha,beta,gamma_x,gamma_y,tau_1x,tau_1y,tau_2x,tau_2y,psi_x,psi_y] = drop_ax_adjoint_coefficients_alex ... 
%(inv_capl,velx,vely,vnx,vny,vtx,vty,s,crvm,dvnx_ds,dvny_ds,dp_dn,n_du_n,dcrvm_vnx,dcrvm_vny,NSG);
%beta = smooth(NSG,beta,1);
%
%gamma_x = smooth(NSG,gamma_x,1);
%gamma_y = smooth(NSG,gamma_y,1);
%
%tau_1x = smooth(NSG,tau_1x,1);
%tau_1y = smooth(NSG,tau_1y,1);
%
%tau_2x = smooth(NSG,tau_2x,1);
%tau_2y = smooth(NSG,tau_2y,1);
%
%psi_x = smooth(NSG,psi_x,1);
%psi_y = smooth(NSG,psi_y,1);
%
% ************** construction of SLP_x and SLP_y matrices *****************
[SLP_x,SLP_y] = drop_ax_adjoint_blocks_slp...
    (NSG,XI,YI,vnx,vny,vtx,vty, ...
    Iflow,wall,sc,RL,Nsum,Np);


% ***************** construction of the identity (I) matrix **************
Ix = eye(NSG+1);
Iy = eye(NSG+1);

% ************** construction of Gx, Gy and T matrices *************
[G_x,G_y,TM] = drop_ax_adjoint_blocks_v2 ...
(NSG,ds,alpha,beta,gamma_x,gamma_y,tau_1x,tau_1y,tau_2x,tau_2y,psi_x,psi_y,s,tau,vnx,vny);

%[G_x,G_y,TM] = drop_ax_adjoint_blocks_alex ...
%(NSG,ds,alpha,beta,gamma_x,gamma_y,tau_1x,tau_1y,tau_2x,tau_2y,psi_x,psi_y,inv_capl,...
%vnx,vny,vtx,vty,crvm);
%
%nv(1,:) = vnx(1:NSG+1);
%nv(2,:) = vny(1:NSG+1); 
%uv(1,:) = velx(1:NSG+1);
%uv(2,:) = vely(1:NSG+1);


% -----------------------------------------------------------
%[TM, G_x, G_y] = st_adjoint_xstar_matrix(nv, crvm, uv, capl)
% -----------------------------------------------------------



% ********************** construction of the BIG matrix (BM) ***********************
BM = [Ix,zeros(NSG+1,NSG+1),-SLP_x;zeros(NSG+1,NSG+1),Iy,-SLP_y;...
      G_x,G_y,TM];
size(BM)

% ********************** constructing the RHS vector ********************************
% 1. Third column
gamma_vec = zeros(NSG+1,1);

for i=1:NSG+1

  velx_diff_i = velx(i) - velx_ref(i);
  vely_diff_i = vely(i) - vely_ref(i);
  D2_i = velx_diff_i^2 + vely_diff_i^2;

  C_x(i) = (velx(i) - velx_ref(i))*du_dn(i);
  C_y(i) = (vely(i) - vely_ref(i))*dv_dn(i);
  gamma_vec(i,1) = C_x(i) + C_y(i) + crvm(i)*D2_i;

end

%figure(22)
%%plot(XI,C_x,XI,C_y)
%plot(XI,gamma_vec);
%%xlabel('XI')
%%legend('Cx','Cy')
%legend('RHS')

% 2. First two columns
[RHS_vecx,RHS_vecy] = drop_ax_adjoint_rhs_slp_xy ...
     (NSG,NGL,XI,YI,Iflow,wall,sc,RL,Nsum,Np,Dfx,Dfy);

RHS_vec = [RHS_vecx;RHS_vecy;gamma_vec];
size(RHS_vec)

% ************ Obtaining k*, u* and v* **************
%solution = BM\RHS_vec;
[solution,flag,res,iter] = gmres(BM,RHS_vec,[],[],200);

adj_velx = solution(1:NSG+1);
adj_vely = solution(NSG+2:2*NSG+2);


% ** printing the solution vector
fprintf('solution vector\n')
cond(BM)
%solution
flag
res
iter

% ** printing the condition numbers

fprintf('condition numbers of SLP_x, SLP_y, G_x, G_y, TM\n')
cond(SLP_x)
cond(SLP_y)
cond(G_x)
cond(G_y)
cond(TM)


fprintf('k_star\n');
k_star = solution(2*NSG+3:end)

fprintf('adj_velx\n');
adj_velx
fprintf('adj_vely\n');
adj_vely

%half = (NSG+1)/2;
%for i=1:half
%  adj_vely(half+1-i) = adj_vely(half+i);
%end
%
% ** Plotting k*, u* and v*
figure(24)
plot(XI,k_star,'LineWidth',3)
xlabel('x')
ylabel('k*')

figure(23)
adj_plot = plot(XI,adj_velx,XI,adj_vely,'LineWidth',3)
set(adj_plot(1),'linewidth',3);
set(adj_plot(2),'linewidth',3);
xlabel('x')
legend('u*','v*')

% ** printing the eigenvalues

fprintf('eigenvalues of SLP_x, SLP_y, G_x, G_y, TM\n');
EV_SLP_x = eig(SLP_x);
EV_SLP_y = eig(SLP_y);
EV_G_x = eig(G_x);
EV_G_y = eig(G_y);
EV_TM = eig(TM);


% ** plotting the eigenvalues

figure(11)
plot(real(EV_SLP_x),imag(EV_SLP_x),'o')

xlabel('Re(\lambda)')
ylabel('Im(\lambda)')

figure(12)
plot(real(EV_SLP_y),imag(EV_SLP_y),'o')

xlabel('Re(\lambda)')
ylabel('Im(\lambda)')

figure(13)
plot(real(EV_G_x),imag(EV_G_x),'o')

xlabel('Re(\lambda)')
ylabel('Im(\lambda)')

figure(14)
plot(real(EV_G_y),imag(EV_G_y),'o')

xlabel('Re(\lambda)')
ylabel('Im(\lambda)')

figure(15)
plot(real(EV_TM),imag(EV_TM),'o')

xlabel('Re(\lambda)')
ylabel('Im(\lambda)')


% ********************* Computing the adjoint gradient ************************

adj_velx = smooth(NSG,adj_velx,2);
adj_vely = smooth(NSG,adj_vely,2);

grad = 0.0;

for i=1:NSG

    adj_vel_i = (adj_velx(i)*vnx(i)+adj_vely(i)*vny(i));
    adj_vel_ip1 = (adj_velx(i+1)*vnx(i+1)+adj_vely(i+1)*vny(i+1));
    adj_vel_avg(i) = 0.5*(adj_vel_i + adj_vel_ip1);

    crvm_avg(i) = 0.5*(crvm(i) + crvm(i+1));
 
    DX = XI(i+1)-XI(i);
    DY = YI(i+1)-YI(i);
    dls = sqrt(DX^2 + DY^2);

    grad = grad - (1.0/capl^2)*0.5*( (crvm(i)*(adj_vel_i)) ...
                       +   (crvm(i+1)*(adj_vel_ip1)) )*dls;

end

% steepest descent method with constant step size
grad
alpha=2.5E-7;

capl = capl - alpha*grad


RHS_vec
%nv(1,:)

% Rank of block matrices
rank(SLP_x)
rank(SLP_y)
rank(G_x)
rank(G_y)
rank(TM)

%=========
end
%=========
