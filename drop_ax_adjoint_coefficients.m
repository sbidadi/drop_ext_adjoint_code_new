function [alpha,beta,gamma_x,gamma_y,tau_1x,tau_1y,tau_2x,tau_2y,psi_x,psi_y, tau] = drop_ax_adjoint_coefficients ... 
(inv_capl,velx,vely,vnx,vny,vtx,vty,s,crvm,...
dvnx_ds,dvny_ds,dp_dn,n_du_n,dcrvm_vnx,dcrvm_vny,NSG)



% 1: Computing alpha, beta and gamma 
beta_2 = n_du_n;

% coefficient alpha 
for i=1:NSG+1

  alpha(i) = velx(i)*vtx(i) + vely(i)*vty(i);
end

% coefficient beta
for i=1:NSG+1

  if (i == 1)
%     beta_1(i) = (alpha(i+1) - alpha(i))/(s(i+1)-s(i));
      beta_1(i) = 0.0;

  elseif (i == NSG+1)
%     beta_1(i) = (alpha(i-1) - alpha(i))/(s(i-1)-s(i));
      beta_1(i) = 0.0;
  
  else 
     beta_1(i) = (alpha(i+1) - alpha(i-1))/(s(i+1) - s(i-1));

  end

  beta(i) = beta_1(i) + beta_2(i) ;

  % computing gamma
  gamma_x(i) = dp_dn(i)*vnx(i);
  gamma_y(i) = dp_dn(i)*vny(i);

  coeff(i) = dcrvm_vnx(i)*vtx(i)+dcrvm_vny(i)*vty(i);
 
end

% computing d_ds(dk/dn * t)
for i = 1:NSG+1

   if (i == 1)

     dcoeff_ds(i) = (coeff(i+1) - coeff(i))/(s(i+1) - s(i));

   elseif (i == NSG+1)

     dcoeff_ds(i) = (coeff(i) - coeff(i-1))/(s(i) - s(i-1));

   else

     dcoeff_ds(i) = (coeff(i+1) - coeff(i-1))/(s(i+1) - s(i-1));

   end

end

% *************** computing tau_1x, tau_1y, psi_x and psi_y

for i=1:NSG+1

    tau_1x(i) = inv_capl*coeff(i)*dvnx_ds(i);
    tau_1y(i) = inv_capl*coeff(i)*dvny_ds(i);

    tau_2x(i) = inv_capl*coeff(i)*vnx(i);
    tau_2y(i) = inv_capl*coeff(i)*vny(i);

    tau(i) = inv_capl*coeff(i);
    tau(i) = inv_capl*coeff(i);

    psi_x(i) = inv_capl*vnx(i)*dcoeff_ds(i);
    psi_y(i) = inv_capl*vny(i)*dcoeff_ds(i);

end

vec(:,1) = alpha;
vec(:,2) = beta; 
vec(:,3) = gamma_x;
vec(:,4) = gamma_y;
vec(:,5) = tau_1x;
vec(:,6) = tau_1y;
vec(:,7) = psi_x;
vec(:,8) = psi_y;
vec(:,9) = tau_2x;
vec(:,10) = tau_2y;

vec

end
