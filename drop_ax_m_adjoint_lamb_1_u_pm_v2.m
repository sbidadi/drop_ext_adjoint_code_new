function [X0_vec_os,Y0_vec_os,velx_os,vely_os] = drop_ax_m_adjoint_lamb_1_u_pm_v2 ...
               (NSG,NGL,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
                Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s)

  cf = -1/(8*pi*capl);

  for i=1:NSG+1

    Dfx(i) = crvm(i)*vnx(i);
    Dfy(i) = crvm(i)*vny(i);

  end


  for j=1:NSG+1

    X0 = XI(j);
    Y0 = YI(j);
    
    half = NSG/2;

    if (j <= half)

      if (j == 1)

        X0_j(2) = X0;
        Y0_j(2) = Y0 + YI(j+1);

        DX0 = X0 - XI(j+1);
        X0_j(3) = X0 - DX0;
        Y0_j(3) = Y0;

        DX0 = X0 - XI(j+1);
        X0_j(1) = X0 + DX0;
        Y0_j(1) = Y0;              

        X0_j(4) = X0;
        Y0_j(4) = Y0 - YI(j+1);

     else

        DX0 = XI(j-1) - X0;
        X0_j(1) = X0 + DX0;
        Y0_j(1) = Y0;

        X0_j(2) = X0;
        DY0 = YI(j+1) - Y0;
        Y0_j(2) = Y0 + DY0;

        DX0 = X0 - XI(j+1);
        X0_j(3) = X0 - DX0;
        Y0_j(3) = Y0;
 
        X0_j(4) = X0;
        DY0 = Y0 - YI(j-1);
        Y0_j(4) = Y0 - DY0;

      end

    elseif (j == half+1)

        DX0 = XI(j-1) - X0;
        X0_j(1) = X0 + DX0;
        Y0_j(1) = Y0;

        X0_j(2) = X0;
        DY0 = Y0 - YI(j+1);
        Y0_j(2) = Y0 + DY0;

        DX0 = X0 - XI(j+1);
        X0_j(3) = X0 - DX0;
        Y0_j(3) = Y0; 
        
        X0_j(4) = X0;
        DY0 = Y0 - YI(j-1);
        Y0_j(4) = Y0 - DY0;

    else

      if (j == NSG+1)

        DX0 = X0 - XI(j-1);
        X0_j(3) = X0 - DX0;
        Y0_j(3) = Y0;

        X0_j(2) = X0;
        DY0 = YI(j-1) - Y0;
        Y0_j(2) = Y0 + DY0;

        DX0 = X0 - XI(j-1);
        X0_j(1) = X0 + DX0;
        Y0_j(1) = Y0;

        X0_j(4) = X0;
        DY0 = Y0 - YI(j-1);
        Y0_j(4) = Y0 + DY0;


     else

        DX0 = X0 - XI(j-1);
        X0_j(3) = X0 - DX0;
        Y0_j(3) = Y0;

        X0_j(2) = X0;
        DY0 = YI(j-1) - Y0;
        Y0_j(2) = Y0 + DY0;

        DX0 = X0 - XI(j+1);
        X0_j(1) = X0 - DX0;
        Y0_j(1) = Y0;

        X0_j(4) = X0;
        DY0 = Y0 - YI(j+1);
        Y0_j(4) = Y0 - DY0;

      end

    end

    for k=1:4

     vx_slp = 0.0;
     vy_slp = 0.0;

     for i = 1:NSG

       X1 = XI(i);
       Y1 = YI(i);
       X2 = XI(i+1);
       Y2 = YI(i+1);

       Ising=0;

       if(j==i | j==i+1)
         Ising=1;
       end
      
      [Qxx,Qxs ...
        ,Qsx,Qss ...
       ] = drop_ax_slp_line ...
       ...
       (X0_j(k),Y0_j(k) ...
       ,X1,Y1 ...
       ,X2,Y2 ...
       ,NGL ...
       ,Iflow ...
       ,wall ...
       ,sc      ...
       ,RL      ...
       ,Nsum,Np ...
       ,Ising);


       Dfx_seg = 0.5*(Dfx(i) + Dfx(i+1));
       Dfy_seg = 0.5*(Dfy(i) + Dfy(i+1));

       vx_slp = vx_slp + Qxx*Dfx_seg + Qxs*Dfy_seg;
       vy_slp = vy_slp + Qsx*Dfx_seg + Qss*Dfy_seg;

     end % end of i (segments)

     velx_os(j,k) = cf*vx_slp;
     vely_os(j,k) = cf*vy_slp;
 
     X0_vec_os(j,k) = X0_j(k); 
     Y0_vec_os(j,k) = Y0_j(k);

    end % end of k (= j1,j2,j3,j4)

  end % end of j (nodes)

%  velx_os(1,1) = velx_os(2,1);
%  vely_os(1,1) = vely_os(2,1);
%
%  velx_os(1,2) = velx_os(2,2);
%  vely_os(1,2) = vely_os(2,2);
%
%  velx_os(1,3) = velx_os(2,3);
%  vely_os(1,3) = vely_os(2,3);
% 
%  velx_os(1,4) = velx_os(1,2);
%  vely_os(1,4) = -vely_os(1,2);

%  velx_os(1,4) = velx(1);
%  vely_os(1,4) = vely(1);



%  velx_os(NSG+1,1) = velx_os(NSG,1);
%  vely_os(NSG+1,1) = vely_os(NSG,1);
%
%  velx_os(NSG+1,2) = velx_os(NSG,2);
%  vely_os(NSG+1,2) = vely_os(NSG,2);
%
%  velx_os(NSG+1,3) = velx_os(NSG,3);
%  vely_os(NSG+1,3) = vely_os(NSG,3);
% 
%  velx_os(NSG+1,4) = velx_os(NSG+1,2);
%  vely_os(NSG+1,4) = -vely_os(NSG+1,2);

%  velx_os(NSG+1,4) = velx_os(NSG+1,3);
%  vely_os(NSG+1,4) = vely_os(NSG+1,3);

%  velx_os(NSG+1,4) = velx(NSG+1);
%  vely_os(NSG+1,4) = vely(NSG+1);

end





