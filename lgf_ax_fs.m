function [Gx,Gs] = lgf_ax_fs (x,s,x0,s0)

%-----------------------------------------
% Free-space axisymmetric Green's function.
% of Laplace's equation
%
%  Iopt =  1 compute only the Green's function
%       ne 1 compute Green's function and gradient
%-------------------------------------------

%----------
% constants
%----------
      eps=0.000001;
      pi4 = 4.0D0*pi;
      pi2 = 2.0D0*pi;

%-------- 
% prepare
%-------- 

      Dx = -(x-x0);
      Dxs = Dx*Dx;

      Ds = -(s-s0);
      Dss = Ds*Ds;

      ss0s = (s+s0)^2;

      rks = 4.0*s*s0/(Dxs+ss0s);

      [F,E] = ell_int_laplace(rks);
      
%------------------------
% field point on x axis ?
%------------------------

      Iaxis = 0; % point x0 off the axis

      if(s0 == eps)
        Iaxis = 1;
      end

%-----------------
% Green's function
%-----------------

      if (Iaxis == 1)

        RI10 = pi2/sqrt(Dxs+Dss);
    
      else

        RJ10 = F;
        den = sqrt(Dxs+ss0s);
        RI10 = 4.0*RJ10/den;

      end 

      G = RI10/pi4;

%---------------------
% compute: I10, I30 and I31
%---------------------

      if (Iaxis == 1)

        RI30 = pi2/(sqrt(Dxs+Dss)^3);
        RI31 = 0.0;

      else

        rksc = 1.0-rks;
        RJ30 = E/rksc;
        RJ31 = (-2.0*F+(2.0-rks)*E/rksc)/rks;
        cf   = 4.0/(den^3);
        RI30 = cf*RJ30;
        RI31 = cf*RJ31;

      end 

%---------
% gradient
%---------

      Gx =  - Dx * RI30;
      Gs =  - s*RI30+s0*RI31;

      Gx = 2.0*Gx;
      Gs = 2.0*Gs;

%-----
% Done
%-----

end
