function [RHS_vecx,RHS_vecy] = drop_ax_adjoint_rhs_slp_xy ...
     (NSG,NGL,XI,YI,Iflow,wall,sc,RL,Nsum,Np,Dfx,Dfy)

RHS_vecx(1:NSG+1,1) = 0.0;
RHS_vecy(1:NSG+1,1) = 0.0;


% *************************************
cf= -1/(8.0*pi);

%---
 for j=1:NSG+1  % run over nodes
%---

 X0   =  XI(j);
 Y0   =  YI(j);

 sum1 = 0.0;  % to test identities
 sum2 = 0.0;

 A_x = 0.0;
 A_y = 0.0;

%---
 for i=1:NSG
%---

 X1 = XI(i);
 Y1 = YI(i);
 X2 = XI(i+1);
 Y2 = YI(i+1);

 Ising=0;

 if(j==i | j==i+1)
     Ising=1;
 end

 [Qxx,Qxs ...
 ,Qsx,Qss ...
 ] = drop_ax_slp_line ...
 ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising);

  % storing the Green's function in a matrix 
  Qxx_v(j,i) = Qxx;
  Qxy_v(j,i) = Qxs;
  Qyx_v(j,i) = Qsx;
  Qyy_v(j,i) = Qss;

  Dfx_seg = 0.5*(Dfx(i) + Dfx(i+1));
  Dfy_seg = 0.5*(Dfy(i) + Dfy(i+1));

  A_x = A_x + Qxx*Dfx_seg + Qxs*Dfy_seg;
  A_y = A_y + Qsx*Dfx_seg + Qss*Dfy_seg;

%---
  end % of interfacial segments
%---


  sum1=sum1*cf;  % should be zero
  sum2=sum2*cf;

  RHS_vecx(j,1) = A_x*cf;
  RHS_vecy(j,1) = A_y*cf;

%---
 end % of nodes
%---

end
