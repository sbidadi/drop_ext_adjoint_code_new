function [pr_vec_os, prc_vec_os, X0c_vec_os, Y0c_vec_os, qx, qy] = drop_ax_m_adjoint_lamb_1_p_pm_v2 ...
               (NSG,NGL,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
                Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s)

  cf = -1/(8*pi*capl);
  half = (NSG+1)/2;

  for i=1:NSG+1

    Dfx(i) = crvm(i)*vnx(i);
    Dfy(i) = crvm(i)*vny(i);

    qx(i) = cf*Dfx(i);
    qy(i) = cf*Dfy(i);

  end

  for j=1:NSG+1

    X0 = XI(j);
    Y0 = YI(j);
    
    if (j <= half)

      if (j == 1)

        X0_j(2) = X0;
        X0_jc(2) = X0;
        Y0_j(2) = Y0 + YI(j+1);
        Y0_jc(2) = Y0 + (YI(j+1)/2);

        DX0 = X0 - XI(j+1);
        X0_j(3) = X0 - DX0;
        Y0_j(3) = Y0;
        X0_jc(3) = X0 - (DX0/2);
        Y0_jc(3) = Y0;

        DX0 = X0 - XI(j+1);
        X0_j(1) = X0 + DX0;
        X0_jc(1) = X0 + (DX0/2);
        Y0_j(1) = Y0;
        Y0_jc(1) = Y0_j(1);

        X0_j(4) = X0;
	X0_jc(4) = X0;
        Y0_j(4) = Y0 - YI(j+1);
        Y0_jc(4) = Y0 - (YI(j+1)/2);        
      
    else

        DX0 = XI(j-1) - X0;
        X0_j(1) = X0 + DX0;
        X0_jc(1) = X0 + (DX0/2);
        Y0_j(1) = Y0;
        Y0_jc(1) = Y0;

        X0_j(2) = X0;
        X0_jc(2) = X0;
        DY0 = YI(j+1) - Y0;
        Y0_j(2) = Y0 + DY0;
        Y0_jc(2) = Y0 + (DY0/2);
       
        DX0 = X0 - XI(j+1);
        X0_j(3) = X0 - DX0;
        X0_jc(3) = X0 - (DX0/2);
        Y0_j(3) = Y0;
        Y0_jc(3) = Y0;

        X0_j(4) = X0;
        X0_jc(4) = X0;
        DY0 = Y0 - YI(j-1);
        Y0_j(4) = Y0 - DY0;
        Y0_jc(4) = Y0 - (DY0/2);
 
      end

    elseif (j == half+1)

        DX0 = XI(j-1) - X0;
        X0_j(1) = X0 + DX0;
        X0_jc(1) = X0 + DX0/2;
        Y0_j(1) = Y0;

        DX0 = X0 - XI(j+1);
        X0_j(3) = X0 - DX0;
        X0_j(3) = X0 - DX0/2;
        Y0_j(3) = Y0;

        X0_j(4) = X0;
        DY0 = Y0 - 2.0*YI(j-1);
        Y0_j(4) = Y0 - DY0;
        Y0_jc(4) = Y0 - DY0/2;

        X0_j(2) = X0;
        Y0_j(2) = Y0 + DY0;
        Y0_jc(2) = Y0 + DY0/2;

    else

      if (j == NSG+1)

        DX0 = X0 - XI(j-1);
        X0_j(3) = X0 - DX0;
        X0_jc(3) = X0 - (DX0/2);
        Y0_j(3) = Y0;
        Y0_jc(3) = Y0;

        X0_j(2) = X0;
        X0_jc(2) = X0;
        DY0 = YI(j-1) - Y0;
        Y0_j(2) = Y0 + DY0;
        Y0_jc(2) = Y0 + (DY0/2); 

        DX0 = X0 - XI(j-1);
        X0_j(1) = X0 + DX0;
        X0_jc(1) = X0 + (DX0/2);
        Y0_j(1) = Y0;
        Y0_jc(1) = Y0;

        X0_j(4) = X0;
        X0_jc(4) = X0;
        DY0 = Y0 - YI(j-1);
        Y0_j(4) = Y0 + DY0;
        Y0_jc(4) = Y0 + (DY0/2);

     else

        DX0 = X0 - XI(j-1);
        X0_j(3) = X0 - DX0;
        X0_jc(3) = X0 - (DX0/2);
        Y0_j(3) = Y0;
        Y0_jc(3) = Y0;

        X0_j(2) = X0;
        X0_jc(2) = X0;
        DY0 = YI(j-1) - Y0;
        Y0_j(2) = Y0 + DY0;
        Y0_jc(2) = Y0 + (DY0/2);

        DX0 = X0 - XI(j+1);
        X0_j(1) = X0 - DX0;
        X0_jc(1) = X0 - (DX0/2);
        Y0_j(1) = Y0;
        Y0_jc(1) = Y0;

        X0_j(4) = X0;
        X0_jc(4) = X0;
        DY0 = Y0 - YI(j+1);
        Y0_j(4) = Y0 - DY0;
        Y0_jc(4) = Y0 - (DY0/2);

      end

    end

    for k=1:4

     pr = 0.0;
     prc = 0.0;

     for i = 1:NSG

       X1 = XI(i);
       Y1 = YI(i);
       X2 = XI(i+1);
       Y2 = YI(i+1);

       Ising=0;

       if(j==i | j==i+1)
         Ising=1;
       end
      
       [Px,Ps] = drop_ax_dlp_line_pressure ...
       (X0_j(k),Y0_j(k) ...
       ,X1,Y1 ...
       ,X2,Y2 ...
       ,NGL);

       [Pxc,Psc] = drop_ax_dlp_line_pressure ...
       (X0_jc(k),Y0_jc(k) ...
       ,X1,Y1 ...
       ,X2,Y2 ...
       ,NGL);

       qx_seg = 0.5*(qx(i) + qx(i+1));
       qy_seg = 0.5*(qy(i) + qy(i+1));

       pr = pr + Px*qx_seg + Ps*qy_seg;
       prc = prc + Pxc*qx_seg + Psc*qy_seg;

     end % end of i (segments)

%     fprintf('%3.6f\t%3.6f\n',Px,Ps);
     pr_vec_os(j,k) = pr;
     prc_vec_os(j,k) = prc;

     X0c_vec_os(j,k) = X0_jc(k);
     Y0c_vec_os(j,k) = Y0_jc(k);

    end % end of k

  end % end of j (nodes)

%  pr_vec_os(1,4) = pr_vec_os(1,3);
%  pr_vec_os(NSG+1,4) = pr_vec_os(NSG+1,3);

%  pr_vec_os(1,1) = pr_vec_os(2,1);
%  pr_vec_os(1,2) = pr_vec_os(2,2);
%  pr_vec_os(1,3) = pr_vec_os(2,3);
%  pr_vec_os(1,4) = pr_vec_os(1,2);

%  pr_vec_os(NSG+1,1) = pr_vec_os(NSG,1);
%  pr_vec_os(NSG+1,2) = pr_vec_os(NSG,2);
%  pr_vec_os(NSG+1,3) = pr_vec_os(NSG,3);
%  pr_vec_os(NSG+1,4) = pr_vec_os(NSG+1,2);

%  prc_vec_os(1,1) = prc_vec_os(2,1);
%  prc_vec_os(1,2) = prc_vec_os(2,2);
%  prc_vec_os(1,3) = prc_vec_os(2,3);
%  prc_vec_os(1,4) = prc_vec_os(1,2);

%  prc_vec_os(NSG+1,1) = prc_vec_os(NSG,1);
%  prc_vec_os(NSG+1,2) = prc_vec_os(NSG,2);
%  prc_vec_os(NSG+1,3) = prc_vec_os(NSG,3);
%  prc_vec_os(NSG+1,4) = prc_vec_os(NSG+1,2);



end


