function [capl,grad] = drop_ax_m_adjoint_lamb_1_N_30 ...
    (NSG,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
    Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s)

format long g
NGL = 6;

inv_capl = capl^(-1.0);
hf = 0.5;

for j=1:NSG+1

  Dfx(j) = (velx(j) - velx_ref(j));
  Dfy(j) = (vely(j) - vely_ref(j));

  % computing the coefficients alpha and gamma (lambda=1)
  alpha(j) = velx(j)*vtx(j) + vely(j)*vty(j);


  if (j == 1)
    ds(j) = s(j+1) - s(j);

  elseif (j == NSG+1)
    ds(j) = s(j)-s(j-1);

  else
    ds(j) = s(j+1) - s(j-1);

   end
end


% computing the gradient 
grad = 0.0;
sigma = 0; % size of the control

% ***************** Computing the necessary gradients ****************
for i=1:NSG

   if (i ~= 1)

     velxx(i) = (velx(i+1)-velx(i-1))/(XI(i+1)-XI(i-1));
     velxy(i) = (velx(i+1)-velx(i-1))/(YI(i+1)-YI(i-1));
     velyx(i) = (vely(i+1)-vely(i-1))/(XI(i+1)-XI(i-1));
     velyy(i) = (vely(i+1)-vely(i-1))/(YI(i+1)-YI(i-1));
    
     velx_nx(i) = velxx(i)*vnx(i) + velxy(i)*vny(i);
     vely_ny(i) = velyx(i)*vnx(i) + velyy(i)*vny(i);

     n_vel_n(i) = velx_nx(i)*vnx(i) + vely_ny(i)*vny(i);

%     dvelx_dnx(i) = (velx(i+1)-velx(i-1))/(vnx(i+1)-vnx(i-1));
%     dvelx_dny(i) = (velx(i+1)-velx(i-1))/(vny(i+1)-vny(i-1));
%     dvely_dnx(i) = (vely(i+1)-vely(i-1))/(vnx(i+1)-vnx(i-1));
%     dvely_dny(i) = (vely(i+1)-vely(i-1))/(vny(i+1)-vny(i-1));
%
     dcrvm_dx(i) = (crvm(i+1)-crvm(i-1))/(XI(i+1)-XI(i-1));
     dcrvm_dy(i) = (crvm(i+1)-crvm(i-1))/(YI(i+1)-YI(i-1));

     dcrvm_vnx(i) = (crvm(i+1)-crvm(i-1))/(vnx(i+1)-vnx(i-1));
     dcrvm_vny(i) = (crvm(i+1)-crvm(i-1))/(vny(i+1)-vny(i-1));

    else

     velxx(i) = (velx(i+1)-velx(i))/(XI(i+1)-XI(i));
     velxy(i) = (velx(i+1)-velx(i))/(YI(i+1)-YI(i));
     velyx(i) = (vely(i+1)-vely(i))/(XI(i+1)-XI(i));
     velyy(i) = (vely(i+1)-vely(i))/(YI(i+1)-YI(i));
    
     velx_nx(i) = velxx(i)*vnx(i) + velxy(i)*vny(i);
     vely_ny(i) = velyx(i)*vnx(i) + velyy(i)*vny(i);

     n_vel_n(i) = velx_nx(i)*vnx(i) + vely_ny(i)*vny(i);

%     dvelx_dnx(i) = (velx(i+1)-velx(i))/(vnx(i+1)-vnx(i));
%     dvelx_dny(i) = (velx(i+1)-velx(i))/(vny(i+1)-vny(i));
%     dvely_dnx(i) = (vely(i+1)-vely(i))/(vnx(i+1)-vnx(i));
%     dvely_dny(i) = (vely(i+1)-vely(i))/(vny(i+1)-vny(i));
%
     dcrvm_dx(i) = (crvm(i+1)-crvm(i))/(XI(i+1)-XI(i));
     dcrvm_dy(i) = (crvm(i+1)-crvm(i))/(YI(i+1)-YI(i));

     dcrvm_vnx(i) = (crvm(i+1)-crvm(i))/(vnx(i+1)-vnx(i));
     dcrvm_vny(i) = (crvm(i+1)-crvm(i))/(vny(i+1)-vny(i));

    end

end

velxx(NSG+1) = (velx(NSG+1)-velx(NSG))/(XI(NSG+1)-XI(NSG));
velxy(NSG+1) = (velx(NSG+1)-velx(NSG))/(YI(NSG+1)-YI(NSG));
velyx(NSG+1) = (vely(NSG+1)-vely(NSG))/(XI(NSG+1)-XI(NSG));
velyy(NSG+1) = (vely(NSG+1)-vely(NSG))/(YI(NSG+1)-YI(NSG));
    
velx_nx(NSG+1) = velxx(NSG+1)*vnx(NSG+1) + velxy(NSG+1)*vny(NSG+1);
vely_ny(NSG+1) = velyx(NSG+1)*vnx(NSG+1) + velyy(NSG+1)*vny(NSG+1);

n_vel_n(NSG+1) = velx_nx(NSG+1)*vnx(NSG+1) + vely_ny(NSG+1)*vny(NSG+1);

%dvelx_dnx(NSG+1) = (velx(NSG+1)-velx(NSG))/(vnx(NSG+1)-vnx(NSG));
%dvelx_dny(NSG+1) = (velx(NSG+1)-velx(NSG))/(vny(NSG+1)-vny(NSG));
%dvely_dnx(NSG+1) = (vely(NSG+1)-vely(NSG))/(vnx(NSG+1)-vnx(NSG));
%dvely_dny(NSG+1) = (vely(NSG+1)-vely(NSG))/(vny(NSG+1)-vny(NSG));

dcrvm_dx(NSG+1) = (crvm(NSG+1)-crvm(NSG))/(XI(NSG+1)-XI(NSG));
dcrvm_dy(NSG+1) = (crvm(NSG+1)-crvm(NSG))/(YI(NSG+1)-YI(NSG));

dcrvm_vnx(NSG+1) = (crvm(NSG+1)-crvm(NSG))/(vnx(NSG+1)-vnx(NSG));
dcrvm_vny(NSG+1) = (crvm(NSG+1)-crvm(NSG))/(vny(NSG+1)-vny(NSG));


% **************** Computing the coefficient beta *************

beta_2 = n_vel_n;

for i=1:NSG+1

     dp_dn = inv_capl*(vnx(i)*dcrvm_dx(i) + vny(i)*dcrvm_dy(i));
     gamma_x(i) = dp_dn*vnx(i);
     gamma_y(i) = dp_dn*vny(i);
 
  if (i == 1)
     beta_1(i) = (alpha(i+1) - alpha(i))/(s(i+1)-s(i));

  elseif (i == NSG+1)
     beta_1(i) = (alpha(i-1) - alpha(i))/(s(i-1)-s(i));

  else 
     beta_1(i) = (alpha(i+1) - alpha(i-1))/(s(i+1) - s(i-1));

  end

%  beta(i) = beta_1(i) + beta_2(i) - (velx(i)*vnx(i) + vely(i)*vny(i));
  beta(i) = beta_1(i) + beta_2(i) ;

end


% *************************************


cf= 1/(8.0*pi);

%---
 for j=1:NSG+1  % run over nodes
%---

 X0   =  XI(j);
 Y0   =  YI(j);

 sum1 = 0.0;  % to test identities
 sum2 = 0.0;

 A_x = 0.0;
 A_y = 0.0;

%---
 for i=1:NSG
%---

 X1 = XI(i);
 Y1 = YI(i);
 X2 = XI(i+1);
 Y2 = YI(i+1);

 Ising=0;

 if(j==i | j==i+1)
     Ising=1;
 end

 [Qxx,Qxs ...
 ,Qsx,Qss ...
 ] = drop_ax_slp_line ...
 ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising);

  % storing the Green's function in a matrix 
  Qxx_v(j,i) = Qxx;
  Qxy_v(j,i) = Qxs;
  Qyx_v(j,i) = Qsx;
  Qyy_v(j,i) = Qss;  

  vnx0=0.5*(vnx(i)+vnx(i+1));
  vny0=0.5*(vny(i)+vny(i+1));
  vnx0=-YI(i+1)+YI(i);
  vny0= XI(i+1)-XI(i);
  norm = sqrt(vnx0^2+vny0^2);
  vnx0 = vnx0/norm;
  vny0 = vny0/norm;

  sum1 = sum1 + Qxx*vnx0+Qxs*vny0;
  sum2 = sum2 + Qsx*vnx0+Qss*vny0;

  Dfx_seg = 0.5*(Dfx(i) + Dfx(i+1));
  Dfy_seg = 0.5*(Dfy(i) + Dfy(i+1));

  A_x = A_x + Qxx*Dfx_seg + Qxs*Dfy_seg;
  A_y = A_y + Qsx*Dfx_seg + Qss*Dfy_seg;

%---
  end % of interfacial segments
%---

  sum1=sum1*cf;  % should be zero
  sum2=sum2*cf;

  A_x_vec(j) = A_x*cf;
  A_y_vec(j) = A_y*cf;

%---
 end % of nodes
%---

% ***** Computing RHS of the system ****
for i=1:NSG+1

  velx_diff_i = velx(i) - velx_ref(i);
  vely_diff_i = vely(i) - vely_ref(i);
  D2_i = velx_diff_i^2 + vely_diff_i^2;

  C_x = (velx(i) - velx_ref(i))*velx_nx(i); 
  C_y = (vely(i) - vely_ref(i))*vely_ny(i); 
  C_rhs = C_x + C_y + crvm(i)*D2_i;

  RHS_1 = C_rhs; 
  RHS_2 = A_x_vec(i)*gamma_x(i) + A_y_vec(i)*gamma_y(i);
  RHS(i) = (RHS_1 + RHS_2)*ds(i);

end

% ***************** Building the coefficients of the matrix *******************

cf = 1.0/(16*pi);

for i = 1:NSG+1

  for j = 1:NSG+1

    % **** Diagonal terms **** 
    if (i == j)

      if (i == 1)

	vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);	

      	Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
      	Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

      	temp = cf*gamma_x(i)*ds(i)*Mij + cf*gamma_y(i)*ds(i)*Nij;  

        C(i,j) = temp - alpha(i) + beta(i)*ds(i);

      elseif (i == NSG+1)

	vnx_avg = vnx(j) + vnx(j-1);
        vny_avg = vny(j) + vny(j-1);	

      	Mij = vnx_avg*Qxx_v(i,j-1) + vny_avg*Qxy_v(i,j-1);
      	Nij = vnx_avg*Qyx_v(i,j-1) + vny_avg*Qyy_v(i,j-1);

      	temp = cf*gamma_x(i)*ds(i)*Mij + cf*gamma_y(i)*ds(i)*Nij;

        C(i,j) = temp + alpha(i) + beta(i)*ds(i);
		
      else

	vnx_avg_m1 = vnx(j-1) + vnx(j);
        vny_avg_m1 = vny(j-1) + vny(j);	

	vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);	

        Mijm1 = vnx_avg_m1*Qxx_v(i,j-1) + vny_avg_m1*Qxy_v(i,j-1);
        Nijm1 = vnx_avg_m1*Qyx_v(i,j-1) + vny_avg_m1*Qyy_v(i,j-1);

        Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
        Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

        C1 = cf*gamma_x(i)*ds(i)*(Mijm1 + Mij);       
        C2 = cf*gamma_y(i)*ds(i)*(Nijm1 + Nij);  

        C(i,j) = C1 + C2 + beta(i)*ds(i);

      end

    % ****** Off-Diagonal Terms ******
    else

      if (j == 1) % first column

        if (i == 2)

          vnx_avg = vnx(j) + vnx(j+1);
          vny_avg = vny(j) + vny(j+1);

          Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
          Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

          temp = cf*gamma_x(i)*ds(i)*Mij + cf*gamma_y(i)*ds(i)*Nij;

          C(i,j) = temp - alpha(i);

        else

          vnx_avg = vnx(j) + vnx(j+1);
          vny_avg = vny(j) + vny(j+1);

          Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
          Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

          C(i,j) = cf*gamma_x(i)*ds(i)*Mij + cf*gamma_y(i)*ds(i)*Nij;

        end


      elseif (j == NSG+1) % last column

        if (i == NSG) 

          vnx_avg = vnx(j) + vnx(j-1);
          vny_avg = vny(j) + vny(j-1);

          Mijm1 = vnx_avg*Qxx_v(i,j-1) + vny_avg*Qxy_v(i,j-1);
          Nijm1 = vnx_avg*Qyx_v(i,j-1) + vny_avg*Qyy_v(i,j-1);

          temp = cf*gamma_x(i)*ds(i)*Mijm1 + cf*gamma_y(i)*ds(i)*Nijm1;
          
          C(i,j) = temp + alpha(i);
    
        else

          vnx_avg = vnx(j) + vnx(j-1);
          vny_avg = vny(j) + vny(j-1);

          Mijm1 = vnx_avg*Qxx_v(i,j-1) + vny_avg*Qxy_v(i,j-1);
          Nijm1 = vnx_avg*Qyx_v(i,j-1) + vny_avg*Qyy_v(i,j-1);

          C(i,j) = cf*gamma_x(i)*ds(i)*Mijm1 + cf*gamma_y(i)*ds(i)*Nijm1;

        end

      elseif (j == i-1) % left of the diagonal component

        vnx_avg_m1 = vnx(j-1) + vnx(j);
        vny_avg_m1 = vny(j-1) + vny(j);

        vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);

        Mijm1 = vnx_avg_m1*Qxx_v(i,j-1) + vny_avg_m1*Qxy_v(i,j-1);
        Nijm1 = vnx_avg_m1*Qyx_v(i,j-1) + vny_avg_m1*Qyy_v(i,j-1);

        Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
        Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

        C1 = cf*gamma_x(i)*ds(i)*(Mijm1 + Mij);
        C2 = cf*gamma_y(i)*ds(i)*(Nijm1 + Nij);

        C(i,j) = C1 + C2 - alpha(i);
	

      elseif (j == i+1) % right of the diagonal component

        vnx_avg_m1 = vnx(j-1) + vnx(j);
        vny_avg_m1 = vny(j-1) + vny(j);

        vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);

        Mijm1 = vnx_avg_m1*Qxx_v(i,j-1) + vny_avg_m1*Qxy_v(i,j-1);
        Nijm1 = vnx_avg_m1*Qyx_v(i,j-1) + vny_avg_m1*Qyy_v(i,j-1);

        Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
        Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

        C1 = cf*gamma_x(i)*ds(i)*(Mijm1 + Mij);
        C2 = cf*gamma_y(i)*ds(i)*(Nijm1 + Nij);

        C(i,j) = C1 + C2 + alpha(i);

      else % all other components

        vnx_avg_m1 = vnx(j-1) + vnx(j);
        vny_avg_m1 = vny(j-1) + vny(j);

        vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);

        Mijm1 = vnx_avg_m1*Qxx_v(i,j-1) + vny_avg_m1*Qxy_v(i,j-1);
        Nijm1 = vnx_avg_m1*Qyx_v(i,j-1) + vny_avg_m1*Qyy_v(i,j-1);

        Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
        Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

        C1 = cf*gamma_x(i)*ds(i)*(Mijm1 + Mij);
        C2 = cf*gamma_y(i)*ds(i)*(Nijm1 + Nij);

        C(i,j) = C1 + C2;

      end

    end % end for off-diagonal terms

  end % for 'j'

end % for 'i'


% **** Computing k_star ****

cond(C)
size(C)
size(RHS)

%k_star = C\RHS';

[k_star,flag,res,iter] = gmres(C,RHS',[],[],200)
k_star
flag

%---
% adjoint single layer over the interface
% computed over the interface
%---


%**** Adjoint force ****
for i = i: NSG+1
  adj_Dfx(i) = Dfx(i) - k_star(i)*vnx(i);
  adj_Dfy(i) = Dfy(i) - k_star(i)*vny(i);
end

cf= -1/(8.0*pi);
%---
 for j=1:NSG+1  % run over nodes
%---

 X0   =  XI(j);
 Y0   =  YI(j);

 adj_Df_x0 = adj_Dfx(j);
 adj_Df_y0 = adj_Dfy(j);

 sum1 = 0.0;  % to test identities
 sum2 = 0.0;

 vx_slp = 0.0;
 vy_slp = 0.0;

%---
 for i=1:NSG
%---

 X1 = XI(i);
 Y1 = YI(i);
 X2 = XI(i+1);
 Y2 = YI(i+1);

 Ising=0;

 Ichoose = 1;
%---
 if(Ichoose==1)  % lines
%---

   if(j==i | j==i+1)
     Ising=1;
   end

 [Qxx,Qxs ...
 ,Qsx,Qss ...
 ] = drop_ax_slp_line ...
 ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising);

  vnx0=0.5*(vnx(i)+vnx(i+1));
  vny0=0.5*(vny(i)+vny(i+1));
  vnx0=-YI(i+1)+YI(i);
  vny0= XI(i+1)-XI(i);
  norm = sqrt(vnx0^2+vny0^2);
  vnx0 = vnx0/norm;
  vny0 = vny0/norm;

  sum1 = sum1 + Qxx*vnx0+Qxs*vny0;
  sum2 = sum2 + Qsx*vnx0+Qss*vny0;

  adj_Dfx_seg = 0.5*(adj_Dfx(i) + adj_Dfx(i+1));
  adj_Dfy_seg = 0.5*(adj_Dfy(i) + adj_Dfy(i+1));

  vx_slp = vx_slp + Qxx*adj_Dfx_seg + Qxs*adj_Dfy_seg;
  vy_slp = vy_slp + Qsx*adj_Dfx_seg + Qss*adj_Dfy_seg;

%---
 else  % spline
%---

  Ising =1;

  Ax=Axint(i); Bx=Bxint(i); Cx=Cxint(i);
  Ay=Ayint(i); By=Byint(i); Cy=Cyint(i);

  Xint1 = Xint(i); Xint2 =Xint(i+1);
  Dfn1  =  adj_Dfn(i); Dfn2  = adj_Dfn(i+1);
  vnx1  =  vnx(i); vnx2   =vnx(i+1);
  vny1  =  vny(i); vny2   =vny(i+1);

  [Qx,Qy ...
  ,Wx,Wy] = drop_ax_slp_spline ...
  ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Xint1,Xint2 ...
  ,Ax,Bx,Cx ...
  ,Ay,By,Cy ...
  ,Dfn1,Dfn2 ...
  ,vnx1,vnx2 ...
  ,vny1,vny2 ...
  ,Dfn0 ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising ...
  );

  sum1 = sum1 + Qx;
  sum2 = sum2 + Qy;

  vx = vx + Wx;
  vy = vy + Wy;

%---
 end
%---

%---
  end % of interfacial segments
%---

  sum1=sum1*cf;  % should be zero
  sum2=sum2*cf;

  vx_slp = vx_slp*cf;
  vy_slp = vy_slp*cf;

  adj_velx(j) = vx_slp;
  adj_vely(j) = vy_slp;

%---
 end % of nodes
%---



adj_velx'
adj_vely'




% ********************* Computing the adjoint gradient ************************

grad = 0.0;

for i=1:NSG

    adj_vel_i = (adj_velx(i)*vnx(i)+adj_vely(i)*vny(i));
    adj_vel_ip1 = (adj_velx(i+1)*vnx(i+1)+adj_vely(i+1)*vny(i+1));
    adj_vel_avg(i) = 0.5*(adj_vel_i + adj_vel_ip1);

    crvm_avg(i) = 0.5*(crvm(i) + crvm(i+1));
 
    DX = XI(i+1)-XI(i);
    DY = YI(i+1)-YI(i);
    dls = sqrt(DX^2 + DY^2);

%    grad = grad - (1.0/capl^2)*0.5*( (crvm(i)*((Ps(i)-Ps_ref(i)) - n_gadj_vel_n(i))) ...
%                       +   (crvm(i+1)*((Ps(i+1)-Ps_ref(i+1)) - n_gadj_vel_n(i+1) )) )*dl;

%    grad1 = grad1 - (1.0/capl_E^2)*0.5*( (crvm(i)*((Ps(i)-Ps_ref(i)))) ...
%                        +   (crvm(i+1)*((Ps(i+1)-Ps_ref(i+1)) )) )*dl;
%
%
%    grad2 = grad2 + (1.0/capl_E^2)*0.5*( (- n_gadj_vel_n(i)) ...
%                       +   (crvm(i+1)*( - n_gadj_vel_n(i+1) )) )*dl;
%
    grad = grad - (1.0/capl^2)*0.5*( (crvm(i)*(adj_vel_i)) ...
                       +   (crvm(i+1)*(adj_vel_ip1)) )*dls;


%   velx_diff_i = velx(i) - velx_ref(i);
%   vely_diff_i = vely(i) - vely_ref(i);
%   D2_i = velx_diff_i^2 + vely_diff_i^2;
%
%   velx_diff_ip1 = velx(i+1) - velx_ref(i+1);
%   vely_diff_ip1 = vely(i+1) - vely_ref(i+1);
%   D2_ip1 = velx_diff_ip1^2 + vely_diff_ip1^2;
%
%   grad = grad - 0.5*( (0.5*D2_i  + (capl_inv^2)*crvm(i)*adj_vel_i ) ...
%                       + (0.5*D2_ip1 + (capl_inv^2)*crvm(i+1)*adj_vel_ip1 ) )*dl;
%
%

end

% steepest descent method with constant step size
grad
alpha=2.5E-7;
%alpha=4E-8;


% Armijo's rule
%alpha = 0.01
%sigma = 0.001
%beta = 0.25
%
%RHS = CF + sigma*alpha*grad*(-grad);
%LHS = 

capl = capl - alpha*grad

%pause

%=========
end
%=========
