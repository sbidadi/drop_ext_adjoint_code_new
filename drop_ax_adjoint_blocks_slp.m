function [SLP_x,SLP_y] = drop_ax_adjoint_blocks_slp ...
    (NSG,XI,YI,vnx,vny,vtx,vty, ...
    Iflow,wall,sc,RL,Nsum,Np)

NGL = 6;

% ***************** construction of the SLP (single layer potential) matrix *************
SLP_x(1:NSG+1,1:NSG+1) = 0.0;
SLP_y(1:NSG+1,1:NSG+1) = 0.0;

%---
 for j=1:NSG+1  % run over nodes
%---

 X0   =  XI(j);
 Y0   =  YI(j);

 sum1 = 0.0;  % to test identities
 sum2 = 0.0;

 vx = 0.0;
 vy = 0.0;

%---
 for i=1:NSG % run over segments
%---

  X1 = XI(i);
  Y1 = YI(i);
  X2 = XI(i+1);
  Y2 = YI(i+1);

  Ising=0;

  if(j==i | j==i+1)
   Ising=1;
  end


   [Qxx,Qxs ...
   ,Qsx,Qss ...
   ] = drop_ax_slp_line ...
   ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising);

  Qxx_vec(j,i) = Qxx;
  Qxs_vec(j,i) = Qxs;
  Qsx_vec(j,i) = Qsx;
  Qss_vec(j,i) = Qss;


%---
  end % of interfacial segments
%---

%---
 end % of nodes
%---

const = 1.0/(16*pi);

for i = 1:NSG+1

  for j = 1:NSG+1

    if (j == 1)

      SLP_x(i,j) = const*(vnx(j)*Qxx_vec(i,j) + vny(j)*Qxs_vec(i,j));
      SLP_y(i,j) = const*(vnx(j)*Qsx_vec(i,j) + vny(j)*Qss_vec(i,j));

    elseif (j == NSG+1)

      SLP_x(i,j) = const*(vnx(j)*Qxx_vec(i,j-1) + vny(j)*Qxs_vec(i,j-1));
      SLP_y(i,j) = const*(vnx(j)*Qsx_vec(i,j-1) + vny(j)*Qss_vec(i,j-1));

    else

      SLP_x(i,j) = const*(vnx(j)*Qxx_vec(i,j-1) + vny(j)*Qxs_vec(i,j-1) + ...
                 vnx(j)*Qxx_vec(i,j) + vny(j)*Qxs_vec(i,j));
      SLP_y(i,j) = const*(vnx(j)*Qsx_vec(i,j-1) + vny(j)*Qss_vec(i,j-1) + ...
                 vnx(j)*Qsx_vec(i,j) + vny(j)*Qss_vec(i,j));

     end

  end

end

end

