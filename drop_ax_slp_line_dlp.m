function [Cx,Cy] = pendant_slp_line_dlp  ...
...
  (X0,Y0 ...
  ,X1,Y1...
  ,X2,Y2...
  ,NGL ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising ...
  ,velxsave ...
  ,velysave ...
  ,velxp1save ...
  ,velyp1save ...
  ,velx0save ...
  ,vely0save ...
  ,vnx ...
  ,vny)

% function [Cx,Cy] = pendant_slp_line_dlp  ...
% ...
%   (X0,Y0 ...
%   ,X1,Y1...
%   ,X2,Y2...
%   ,NGL ...
%   ,Iflow ...
%   ,wall ...
%   ,sc      ...
%   ,RL      ...
%   ,Nsum,Np ...
%   ,Ising ...
%   ,velxsave ...
%   ,velysave ...
%   ,velxp1save ...
%   ,velyp1save ...
%   ,velx0save ...
%   ,vely0save ...
%   ,vnx1,vnx2 ...
%   ,vny1,vny2)

% initialize
% WXXX = 0.0D0;
% WXYX = 0.0D0;
% WXXY = 0.0D0;
% WXYY = 0.0D0;
% WYXX = 0.0D0;
% WYYX = 0.0D0;
% WYXY = 0.0D0;
% WYYY = 0.0D0;
% PWXX = 0.0D0;
% PWXY = 0.0D0;
% PWYX = 0.0D0;
% PWYY = 0.0D0;

Iopt=2;

[ZZ,WW]=gauss_leg(NGL);

%---
% prepare for the quadrature
%---

XM = 0.5D0*(X2+X1);
XD = 0.5D0*(X2-X1);
YM = 0.5D0*(Y2+Y1);
YD = 0.5D0*(Y2-Y1);
DR = sqrt(XD*XD+YD*YD);

%--------------------------
% loop over Gaussian points
%--------------------------
for i=1:NGL

  X = XM+XD*ZZ(i);
  Y = YM+YD*ZZ(i);
 
    % approximating the velocities using linear interpolation
    u(i) = velxsave + ((velxp1save - velxsave)/(X2-X1))*(X - X1);
    v(i) = velysave + ((velyp1save - velysave)/(X2-X1))*(X - X1);

   if(Iflow==1)

   [Wxx,Wxs,Wsx,Wss ...
 ...
   ,QXXX,QXXY,QXYX,QXYY ...
   ,QYXX,QYXY,QYYX,QYYY ...
 ...
   ,PXX,PXY,PYX,PYY ...
   ,Iaxis] ...
 ...
   = sgf_ax_fs (Iopt,X0,Y0,X,Y);

   elseif(Iflow==2)

   [Wxx,Wxs,Wsx,Wss ...
  ...
   ,QXXX,QXXY,QXYX,QXYY ...
   ,QYXX,QYXY,QYYX,QYYY ...
  ...
   ,PXX,PXY,PYX,PYY ...
   ,Iaxis] ...
  ...
    = sgf_ax_w (Iopt,X0,Y0,X,Y,wall);

  elseif(Iflow==3)

   [Wxx,Wxs,Wsx,Wss]  ...
...
   = sgf_ax_1p_ct  ...
...
  (X0,Y0     ...
  ,X,Y   ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  );

  end

%---
% subtract out the log singularity
%---
%  if(Ising==1)
%   Dist2 = (X-X0)^2+(Y-Y0)^2;
%   DD = log(Dist2);
%   Wxx = Wxx + DD;
%   Wss = Wss + DD;
% 
%   QXXX = QXXX + DD;
%   QYYY = QYYY + DD;
% 
%   PXX = PXX + DD;
%   PYY = PYY + DD;
% 
%  end

%---
% carry out the quadrature
%---

WI = WW(i);

%Qxx = Qxx + Wxx * WI;
%Qxs = Qxs + Wxs * WI;
%Qsx = Qsx + Wsx * WI;
%Qss = Qss + Wss * WI;

QXXX_v(i) = QXXX*WI;
QXXY_v(i) = QXXY*WI;
QYXX_v(i) = QYXX*WI;
QYXY_v(i) = QYXY*WI;
QXYX_v(i) = QXYX*WI;
QXYY_v(i) = QXYY*WI;
QYYX_v(i) = QYYX*WI;
QYYY_v(i) = QYYY*WI;

PXX_v(i) = PXX*WI;
PXY_v(i) = PXY*WI;
PYX_v(i) = PYX*WI;
PYY_v(i) = PYY*WI;

% WXXX = WXXX + QXXX*WI;
% WXXY = WXXY + QXXY*WI;
% WYXX = WYXX + QYXX*WI;
% WYXY = WYXY + QYXY*WI;
% WXYX = WXYX + QXYX*WI;
% WXYY = WXYY + QXYY*WI;
% WYYX = WYYX + QYYX*WI;
% WYYY = WYYY + QYYY*WI;
% 
% PWXX = PWXX + PXX*WI;
% PWXY = PWXY + PXY*WI;
% PWYX = PWYX + PYX*WI;
% PWYY = PWYY + PYY*WI;

end


%---
% finish up
%---

%Qxx = Qxx*DR;
%Qxs = Qxs*DR;
%
%Qsx = Qsx*DR;
%Qss = Qss*DR;

for i=1:NGL

 QXXX_v(i) = QXXX_v(i)*DR;
 QXXY_v(i) = QXXY_v(i)*DR;
 QYXX_v(i) = QYXX_v(i)*DR;
 QYXY_v(i) = QYXY_v(i)*DR;
 QXYX_v(i) = QXYX_v(i)*DR;
 QXYY_v(i) = QXYY_v(i)*DR;
 QYYX_v(i) = QYYX_v(i)*DR;
 QYYY_v(i) = QYYY_v(i)*DR;

 PXX_v(i) = PXX_v(i)*DR;
 PXY_v(i) = PXY_v(i)*DR;
 PYX_v(i) = PYX_v(i)*DR;
 PYY_v(i) = PYY_v(i)*DR;

end

% WXXX = WXXX*DR;
% WXXY = WXXY*DR;
% WYXX = WYXX*DR;
% WYXY = WYXY*DR;
% WXYX = WXYX*DR;
% WXYY = WXYY*DR;
% WYYX = WYYX*DR;
% WYYY = WYYY*DR;
% 
% PWXX = PWXX + PXX*DR;
% PWXY = PWXY + PXY*DR;
% PWYX = PWYX + PYX*DR;
% PWYY = PWYY + PYY*DR;

%---------------------
% add back the log singularity
% the point X0 is a segment end-point
%---------------------
% if(Ising==1)
%  SEGL=2.0D0*DR;
% % Qxx = Qxx - 2.0D0* SEGL*(log(SEGL)-1.0D0);
% % Qss = Qss - 2.0D0* SEGL*(log(SEGL)-1.0D0);
% 
%  for i=1:NGL
%   QXXX_v(i) = QXXX_v(i) - 2.0D0* SEGL*(log(SEGL)-1.0D0);
%   QYYY_v(i) = QYYY_v(i) - 2.0D0* SEGL*(log(SEGL)-1.0D0);
%   
%   PXX_v(i) = PXX_v(i) - 2.0D0* SEGL*(log(SEGL)-1.0D0);
%   PYY_v(i) = PYY_v(i) - 2.0D0* SEGL*(log(SEGL)-1.0D0);
%    
%  end
% 
% end

%disp('Computing Cx and Cy')

Cx = 0.0;
Cy = 0.0;

% vnxM = 0.5D0*(vnx2+vnx1);
% vnxD = 0.5D0*(vnx2-vnx1);
% 
% vnyM = 0.5D0*(vny2+vny1);
% vnyD = 0.5D0*(vny2-vny1);


for i = 1:NGL
    
%   vnx  =  vnxM +  vnxD*ZZ(i);
%   vny  =  vnyM +  vnyD*ZZ(i);
%   norm = sqrt(vnx*vnx+vny*vny);
%   vnx=vnx/norm;
%   vny=vny/norm;    

   Cx = Cx + (QXXX_v(i)*(u(i) - velx0save) + QXYX_v(i)*v(i) - PXX_v(i)*vely0save)*vnx ...
	   + (QXXY_v(i)*(u(i) - velx0save) + QXYY_v(i)*v(i) - PXY_v(i)*vely0save)*vny;
   Cy = Cy + (QYXX_v(i)*(u(i) - velx0save) + QYYX_v(i)*v(i) - PYX_v(i)*vely0save)*vnx ...
	   + (QYXY_v(i)*(u(i) - velx0save) + QYYY_v(i)*v(i) - PYY_v(i)*vely0save)*vny;

%   Cx = Cx + (QXXX_v(i)*(u(i) - velx0save) + QXYX_v(i)*(v(i)-vely0save) - PXX_v(i)*vely0save)*vnx ...
%	   + (QXXY_v(i)*(u(i) - velx0save) + QXYY_v(i)*(v(i)-vely0save) - PXY_v(i)*vely0save)*vny;
%   Cy = Cy + (QYXX_v(i)*(u(i) - velx0save) + QYYX_v(i)*(v(i)-vely0save) - PYX_v(i)*vely0save)*vnx ...
%	   + (QYXY_v(i)*(u(i) - velx0save) + QYYY_v(i)*(v(i)-vely0save) - PYY_v(i)*vely0save)*vny;
%
end


%-----
% done
%-----

return
