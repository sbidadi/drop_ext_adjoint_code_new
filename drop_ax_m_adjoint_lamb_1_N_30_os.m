function [capl,grad] = drop_ax_m_adjoint_lamb_1_N_30_os ...
    (NSG,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
    Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s)

format long g
NGL = 6;

inv_capl = capl^(-1.0);
hf = 0.5;

for j=1:NSG+1

  Dfx(j) = (velx(j) - velx_ref(j));
  Dfy(j) = (vely(j) - vely_ref(j));

  % computing the coefficients alpha and gamma (lambda=1)
  alpha(j) = velx(j)*vtx(j) + vely(j)*vty(j);


  if (j == 1)
    ds(j) = s(j+1) - s(j);

  elseif (j == NSG+1)
    ds(j) = s(j)-s(j-1);

  else
    ds(j) = s(j+1) - s(j-1);

   end
end


% computing the gradient 
grad = 0.0;
sigma = 0; % size of the control


[X0_vec_os,Y0_vec_os,velx_os,vely_os] = drop_ax_m_adjoint_lamb_1_u_pm ...
               (NSG,NGL,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
                Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s);

%[pr_vec_os,qx,qy] = drop_ax_m_adjoint_lamb_1_p_pm ...
%               (NSG,NGL,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
%                Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s);
%

[pr_vec_os,prc_vec_os, X0c_vec_os, Y0c_vec_os, qx, qy] = drop_ax_m_adjoint_lamb_1_p_pm ...
               (NSG,NGL,XI,YI,capl,velx,vely,velx_ref,vely_ref,crvm,vnx,vny, ...
                Iflow, wall,sc,RL,Nsum,Np,Ps,Ps_ref,vtx,vty,s);


for i=1:NSG+1

%  for k = 1:4

    velx_1 = velx_os(i,1);
    vely_1 = vely_os(i,1);
    X0_1 = X0_vec_os(i,1);
    Y0_1 = Y0_vec_os(i,1);
    p_1 = pr_vec_os(i,1);


    velx_2 = velx_os(i,2);
    vely_2 = vely_os(i,2);
    X0_2 = X0_vec_os(i,2);
    Y0_2 = Y0_vec_os(i,2);
    p_2 = pr_vec_os(i,2);


    velx_3 = velx_os(i,3);
    vely_3 = vely_os(i,3);
    X0_3 = X0_vec_os(i,3);
    Y0_3 = Y0_vec_os(i,3);
    p_3 = pr_vec_os(i,3);


    velx_4 = velx_os(i,4);
    vely_4 = vely_os(i,4);
    X0_4 = X0_vec_os(i,4);
    Y0_4 = Y0_vec_os(i,4);
    p_4 = pr_vec_os(i,4);

%  end

  % computing n dvel/dn

  velxx(i) = (velx_1 - velx_3)/(X0_1 - X0_3);
  velxy(i) = (velx_2 - velx_4)/(Y0_2 - Y0_4);
  velyx(i) = (vely_1 - vely_3)/(X0_1 - X0_3);
  velyy(i) = (vely_2 - vely_4)/(Y0_2 - Y0_4);

  du_dn(i) = velxx(i)*vnx(i) + velxy(i)*vny(i);
  dv_dn(i) = velyx(i)*vnx(i) + velyy(i)*vny(i);

  n_du_n(i) = du_dn(i)*vnx(i) + dv_dn(i)*vny(i);

  % computing dp/dn
  dp_dx(i) = (p_1 - p_3)/(X0_1 - X0_3);
  dp_dy(i) = (p_2 - p_4)/(Y0_2 - Y0_4);
  dp_dn(i) = dp_dx(i)*vnx(i) + dp_dy(i)*vny(i);

end


%velx_os
%vely_os
%pr_vec_os

du_dn'
dv_dn'
n_du_n'
dp_dn'


% ***************** Computing the necessary gradients ****************
for i = 1:NSG

   if (i ~= 1)

     dcrvm_dx(i) = (crvm(i+1)-crvm(i-1))/(XI(i+1)-XI(i-1));
     dcrvm_dy(i) = (crvm(i+1)-crvm(i-1))/(YI(i+1)-YI(i-1));

     dcrvm_vnx(i) = (crvm(i+1)-crvm(i-1))/(vnx(i+1)-vnx(i-1));
     dcrvm_vny(i) = (crvm(i+1)-crvm(i-1))/(vny(i+1)-vny(i-1));

     dcrvm_ds(i) = (crvm(i+1) - crvm(i-1))/(s(i+1)-s(i-1));

     dvnx_ds(i) = (vnx(i+1)-vnx(i-1))/(s(i+1)-s(i-1));
     dvny_ds(i) = (vny(i+1)-vny(i-1))/(s(i+1)-s(i-1));



    else

     dcrvm_dx(i) = (crvm(i+1)-crvm(i))/(XI(i+1)-XI(i));
     dcrvm_dy(i) = (crvm(i+1)-crvm(i))/(YI(i+1)-YI(i));

     dcrvm_vnx(i) = (crvm(i+1)-crvm(i))/(vnx(i+1)-vnx(i));
     dcrvm_vny(i) = (crvm(i+1)-crvm(i))/(vny(i+1)-vny(i));

     dcrvm_ds(i) = (crvm(i+1) - crvm(i))/(s(i+1)-s(i));

     dvnx_ds(i) = (vnx(i+1)-vnx(i))/(s(i+1)-s(i));
     dvny_ds(i) = (vny(i+1)-vny(i))/(s(i+1)-s(i));

    end

end

dcrvm_dx(NSG+1) = (crvm(NSG+1)-crvm(NSG))/(XI(NSG+1)-XI(NSG));
dcrvm_dy(NSG+1) = (crvm(NSG+1)-crvm(NSG))/(YI(NSG+1)-YI(NSG));

dcrvm_vnx(NSG+1) = (crvm(NSG+1)-crvm(NSG))/(vnx(NSG+1)-vnx(NSG));
dcrvm_vny(NSG+1) = (crvm(NSG+1)-crvm(NSG))/(vny(NSG+1)-vny(NSG));

dcrvm_ds(NSG+1) = (crvm(NSG+1) - crvm(NSG))/(s(NSG+1)-s(NSG));

dvnx_ds(NSG+1) = (vnx(NSG+1)-vnx(NSG))/(s(NSG+1)-s(NSG));
dvny_ds(NSG+1) = (vny(NSG+1)-vny(NSG))/(s(NSG+1)-s(NSG));

%velx_nx
%vely_ny

%dcrvm_vnx(1) = dcrvm_vnx(2);
%dcrvm_vny(1) = dcrvm_vny(2);
%dcrvm_vnx(NSG+1) = dcrvm_vnx(NSG);
%dcrvm_vny(NSG+1) = dcrvm_vny(NSG);

dcrvm_vnx'
dcrvm_vny'

% **************** Computing the coefficient beta *************

beta_2 = n_du_n;

for i=1:NSG+1

  % computing beta
  if (i == 1)
     beta_1(i) = (alpha(i+1) - alpha(i))/(s(i+1)-s(i));
%     temp(i) = (crvm(i+1)*dcrvm_ds(i+1) - crvm(i)*dcrvm_ds(i))/(s(i+1)-s(i));

  elseif (i == NSG+1)
     beta_1(i) = (alpha(i-1) - alpha(i))/(s(i-1)-s(i));
%     temp(i) = (crvm(i)*dcrvm_ds(i) - crvm(i-1)*dcrvm_ds(i-1))/(s(i)-s(i-1));

  else 
     beta_1(i) = (alpha(i+1) - alpha(i-1))/(s(i+1) - s(i-1));
%     temp(i) = (crvm(i+1)*dcrvm_ds(i+1) - crvm(i-1)*dcrvm_ds(i-1))/(s(i+1)-s(i-1));

  end

  beta(i) = beta_1(i) + beta_2(i) ;

  % computing gamma
  gamma_x(i) = dp_dn(i)*vnx(i);
  gamma_y(i) = dp_dn(i)*vny(i);

  coeff(i) = dcrvm_vnx(i)*vtx(i)+dcrvm_vny(i)*vty(i);
 
end


for i = 1:NSG+1

   if (i == 1)

     dcoeff_ds(i) = (coeff(i+1) - coeff(i))/(s(i+1) - s(i));

   elseif (i == NSG+1)

     dcoeff_ds(i) = (coeff(i) - coeff(i-1))/(s(i) - s(i-1));

   else

     dcoeff_ds(i) = (coeff(i+1) - coeff(i-1))/(s(i+1) - s(i-1));

   end

end



for i=1:NSG+1

    tau_1x(i) = inv_capl*coeff(i)*dvnx_ds(i);
    tau_1y(i) = inv_capl*coeff(i)*dvny_ds(i);

    tau_2x(i) = inv_capl*coeff(i)*vnx(i);
    tau_2y(i) = inv_capl*coeff(i)*vny(i);

    psi_x(i) = inv_capl*vnx(i)*dcoeff_ds(i);
    psi_y(i) = inv_capl*vny(i)*dcoeff_ds(i);

end

vec(:,1) = alpha;
vec(:,2) = beta; 
vec(:,3) = gamma_x;
vec(:,4) = gamma_y;
vec(:,5) = tau_1x;
vec(:,6) = tau_1y;
vec(:,7) = psi_x;
vec(:,8) = psi_y;

vec

% *************************************
cf= 1/(8.0*pi);

%---
 for j=1:NSG+1  % run over nodes
%---

 X0   =  XI(j);
 Y0   =  YI(j);

 sum1 = 0.0;  % to test identities
 sum2 = 0.0;

 A_x = 0.0;
 A_y = 0.0;

%---
 for i=1:NSG
%---

 X1 = XI(i);
 Y1 = YI(i);
 X2 = XI(i+1);
 Y2 = YI(i+1);

 Ising=0;

 if(j==i | j==i+1)
     Ising=1;
 end

 [Qxx,Qxs ...
 ,Qsx,Qss ...
 ] = drop_ax_slp_line ...
 ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising);

  % storing the Green's function in a matrix 
  Qxx_v(j,i) = Qxx;
  Qxy_v(j,i) = Qxs;
  Qyx_v(j,i) = Qsx;
  Qyy_v(j,i) = Qss;  

  vnx0=0.5*(vnx(i)+vnx(i+1));
  vny0=0.5*(vny(i)+vny(i+1));
  vnx0=-YI(i+1)+YI(i);
  vny0= XI(i+1)-XI(i);
  norm = sqrt(vnx0^2+vny0^2);
  vnx0 = vnx0/norm;
  vny0 = vny0/norm;

  sum1 = sum1 + Qxx*vnx0+Qxs*vny0;
  sum2 = sum2 + Qsx*vnx0+Qss*vny0;

  Dfx_seg = 0.5*(Dfx(i) + Dfx(i+1));
  Dfy_seg = 0.5*(Dfy(i) + Dfy(i+1));

  A_x = A_x + Qxx*Dfx_seg + Qxs*Dfy_seg;
  A_y = A_y + Qsx*Dfx_seg + Qss*Dfy_seg;

%---
  end % of interfacial segments
%---

  sum1=sum1*cf;  % should be zero
  sum2=sum2*cf;

  A_x_vec(j) = A_x*cf;
  A_y_vec(j) = A_y*cf;

%---
 end % of nodes
%---

% ***** Computing RHS of the system ****
for i=1:NSG+1

  velx_diff_i = velx(i) - velx_ref(i);
  vely_diff_i = vely(i) - vely_ref(i);
  D2_i = velx_diff_i^2 + vely_diff_i^2;

  C_x = (velx(i) - velx_ref(i))*du_dn(i); 
  C_y = (vely(i) - vely_ref(i))*dv_dn(i); 
  C_rhs = C_x + C_y + crvm(i)*D2_i;
%  C_rhs = crvm(i)*D2_i;

  RHS_1 = C_rhs; 
  RHS_2 = A_x_vec(i)*gamma_x(i) + A_y_vec(i)*gamma_y(i);
  RHS_3 = -(A_x_vec(i)*tau_1x(i) + A_y_vec(i)*tau_1y(i));
  RHS_4 = -(A_x_vec(i)*psi_x(i) + A_y_vec(i)*psi_y(i));

  RHS(i) = (RHS_1 + RHS_2 + RHS_3 + RHS_4)*ds(i);
  
end

Ax_vec = A_x_vec'
Ay_vec = A_y_vec'
RHS_vec = RHS'



% ***************** Building the coefficients of the matrix *******************
%tau_1x = zeros(NSG+1);
%tau_1y = zeros(NSG+1);
%psi_x = zeros(NSG+1);
%psi_y = zeros(NSG+1);

dau = zeros(NSG+1);
dav = zeros(NSG+1);

check = 1;
k_star_old = zeros(NSG+1,1);
k_star_new = zeros(NSG+1,1);
k_star = zeros(NSG+1,1);


%while (check > 0) 

cf = 1.0/(16*pi);

for i = 1:NSG+1

  for j = 1:NSG+1

    % **** Diagonal terms **** 
    if (i == j)

      if (i == 1)

	vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);	

      	Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
      	Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

      	temp = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*Mij + ...
               cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*Nij;  

        C(i,j) = temp - alpha(i) + beta(i)*ds(i);

      elseif (i == NSG+1)

	vnx_avg = vnx(j) + vnx(j-1);
        vny_avg = vny(j) + vny(j-1);	

      	Mij = vnx_avg*Qxx_v(i,j-1) + vny_avg*Qxy_v(i,j-1);
      	Nij = vnx_avg*Qyx_v(i,j-1) + vny_avg*Qyy_v(i,j-1);

      	temp = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*Mij + ...
               cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*Nij;

        C(i,j) = temp + alpha(i) + beta(i)*ds(i);
		
      else

	vnx_avg_m1 = vnx(j-1) + vnx(j);
        vny_avg_m1 = vny(j-1) + vny(j);	

	vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);	

        Mijm1 = vnx_avg_m1*Qxx_v(i,j-1) + vny_avg_m1*Qxy_v(i,j-1);
        Nijm1 = vnx_avg_m1*Qyx_v(i,j-1) + vny_avg_m1*Qyy_v(i,j-1);

        Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
        Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

        C1 = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*(Mijm1 + Mij);       
        C2 = cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*(Nijm1 + Nij);  

        C(i,j) = C1 + C2 + beta(i)*ds(i);

      end

    % ****** Off-Diagonal Terms ******
    else

      if (j == 1) % first column

        if (i == 2)

          vnx_avg = vnx(j) + vnx(j+1);
          vny_avg = vny(j) + vny(j+1);

          Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
          Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

          temp = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*Mij + ...
                 cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*Nij;

          C(i,j) = temp - alpha(i);

        else

          vnx_avg = vnx(j) + vnx(j+1);
          vny_avg = vny(j) + vny(j+1);

          Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
          Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

          C(i,j) = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*Mij + ...
                   cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*Nij;

        end


      elseif (j == NSG+1) % last column

        if (i == NSG) 

          vnx_avg = vnx(j) + vnx(j-1);
          vny_avg = vny(j) + vny(j-1);

          Mijm1 = vnx_avg*Qxx_v(i,j-1) + vny_avg*Qxy_v(i,j-1);
          Nijm1 = vnx_avg*Qyx_v(i,j-1) + vny_avg*Qyy_v(i,j-1);

          temp = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*Mijm1 + ...
                 cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*Nijm1;
          
          C(i,j) = temp + alpha(i);
    
        else

          vnx_avg = vnx(j) + vnx(j-1);
          vny_avg = vny(j) + vny(j-1);

          Mijm1 = vnx_avg*Qxx_v(i,j-1) + vny_avg*Qxy_v(i,j-1);
          Nijm1 = vnx_avg*Qyx_v(i,j-1) + vny_avg*Qyy_v(i,j-1);

          C(i,j) = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*Mijm1 + ... 
                   cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*Nijm1;

        end

      elseif (j == i-1) % left of the diagonal component

        vnx_avg_m1 = vnx(j-1) + vnx(j);
        vny_avg_m1 = vny(j-1) + vny(j);

        vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);

        Mijm1 = vnx_avg_m1*Qxx_v(i,j-1) + vny_avg_m1*Qxy_v(i,j-1);
        Nijm1 = vnx_avg_m1*Qyx_v(i,j-1) + vny_avg_m1*Qyy_v(i,j-1);

        Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
        Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

        C1 = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*(Mijm1 + Mij);
        C2 = cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*(Nijm1 + Nij);

        C(i,j) = C1 + C2 - alpha(i);
	

      elseif (j == i+1) % right of the diagonal component

        vnx_avg_m1 = vnx(j-1) + vnx(j);
        vny_avg_m1 = vny(j-1) + vny(j);

        vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);

        Mijm1 = vnx_avg_m1*Qxx_v(i,j-1) + vny_avg_m1*Qxy_v(i,j-1);
        Nijm1 = vnx_avg_m1*Qyx_v(i,j-1) + vny_avg_m1*Qyy_v(i,j-1);

        Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
        Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

        C1 = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*(Mijm1 + Mij);
        C2 = cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*(Nijm1 + Nij);

        C(i,j) = C1 + C2 + alpha(i);

      else % all other components

        vnx_avg_m1 = vnx(j-1) + vnx(j);
        vny_avg_m1 = vny(j-1) + vny(j);

        vnx_avg = vnx(j) + vnx(j+1);
        vny_avg = vny(j) + vny(j+1);

        Mijm1 = vnx_avg_m1*Qxx_v(i,j-1) + vny_avg_m1*Qxy_v(i,j-1);
        Nijm1 = vnx_avg_m1*Qyx_v(i,j-1) + vny_avg_m1*Qyy_v(i,j-1);

        Mij = vnx_avg*Qxx_v(i,j) + vny_avg*Qxy_v(i,j);
        Nij = vnx_avg*Qyx_v(i,j) + vny_avg*Qyy_v(i,j);

        C1 = cf*(gamma_x(i)-tau_1x(i)-psi_x(i))*ds(i)*(Mijm1 + Mij);
        C2 = cf*(gamma_y(i)-tau_1y(i)-psi_y(i))*ds(i)*(Nijm1 + Nij);

        C(i,j) = C1 + C2;

      end

    end % end for off-diagonal terms

  end % for 'j'

end % for 'i'


% **** Computing k_star ****

cond(C)

size(k_star_old)
k_star_old = k_star;
size(k_star_old)
size(k_star)

[k_star,flag,res,iter] = gmres(C,RHS',[],[],200);
k_star
flag

k_star_new = k_star;
size(k_star_new)
size(k_star_old)

diff_k_star = k_star_new - k_star_old;
diff_k_star = abs(diff_k_star);
if (all(abs(diff_k_star(:)) < eps) == 1)
  check == 0;
end


%---
% adjoint single layer over the interface
% computed over the interface
%---


%**** Adjoint force ****
for i = i:NSG+1
  adj_Dfx(i) = Dfx(i) - k_star(i)*vnx(i);
  adj_Dfy(i) = Dfy(i) - k_star(i)*vny(i);
end

cf= -1/(8.0*pi);
%---
 for j=1:NSG+1  % run over nodes
%---

 X0   =  XI(j);
 Y0   =  YI(j);

 adj_Df_x0 = adj_Dfx(j);
 adj_Df_y0 = adj_Dfy(j);

 sum1 = 0.0;  % to test identities
 sum2 = 0.0;

 vx_slp = 0.0;
 vy_slp = 0.0;

%---
 for i=1:NSG
%---

 X1 = XI(i);
 Y1 = YI(i);
 X2 = XI(i+1);
 Y2 = YI(i+1);

 Ising=0;

 Ichoose = 1;
%---
 if(Ichoose==1)  % lines
%---

   if(j==i | j==i+1)
     Ising=1;
   end

 [Qxx,Qxs ...
 ,Qsx,Qss ...
 ] = drop_ax_slp_line ...
 ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising);

  vnx0=0.5*(vnx(i)+vnx(i+1));
  vny0=0.5*(vny(i)+vny(i+1));
  vnx0=-YI(i+1)+YI(i);
  vny0= XI(i+1)-XI(i);
  norm = sqrt(vnx0^2+vny0^2);
  vnx0 = vnx0/norm;
  vny0 = vny0/norm;

  sum1 = sum1 + Qxx*vnx0+Qxs*vny0;
  sum2 = sum2 + Qsx*vnx0+Qss*vny0;

  adj_Dfx_seg = 0.5*(adj_Dfx(i) + adj_Dfx(i+1));
  adj_Dfy_seg = 0.5*(adj_Dfy(i) + adj_Dfy(i+1));

  vx_slp = vx_slp + Qxx*adj_Dfx_seg + Qxs*adj_Dfy_seg;
  vy_slp = vy_slp + Qsx*adj_Dfx_seg + Qss*adj_Dfy_seg;

%---
 else  % spline
%---

  Ising =1;

  Ax=Axint(i); Bx=Bxint(i); Cx=Cxint(i);
  Ay=Ayint(i); By=Byint(i); Cy=Cyint(i);

  Xint1 = Xint(i); Xint2 =Xint(i+1);
  Dfn1  =  adj_Dfn(i); Dfn2  = adj_Dfn(i+1);
  vnx1  =  vnx(i); vnx2   =vnx(i+1);
  vny1  =  vny(i); vny2   =vny(i+1);

  [Qx,Qy ...
  ,Wx,Wy] = drop_ax_slp_spline ...
  ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Xint1,Xint2 ...
  ,Ax,Bx,Cx ...
  ,Ay,By,Cy ...
  ,Dfn1,Dfn2 ...
  ,vnx1,vnx2 ...
  ,vny1,vny2 ...
  ,Dfn0 ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising ...
  );

  sum1 = sum1 + Qx;
  sum2 = sum2 + Qy;

  vx = vx + Wx;
  vy = vy + Wy;

%---
 end
%---

%---
  end % of interfacial segments
%---

  sum1=sum1*cf;  % should be zero
  sum2=sum2*cf;

  vx_slp = vx_slp*cf;
  vy_slp = vy_slp*cf;

  adj_velx(j) = vx_slp;
  adj_vely(j) = vy_slp;

%---
 end % of nodes
%---


adj_velx'
adj_vely'

for i=1:NSG+1

  if (i == 1)

    dau(i) = (adj_velx(i+1) - adj_velx(i))/(s(i+1) - s(i));
    dav(i) = (adj_vely(i+1) - adj_vely(i))/(s(i+1) - s(i));

  elseif (i == NSG+1)

    dau(i) = (adj_velx(i) - adj_velx(i-1))/(s(i) - s(i-1));
    dav(i) = (adj_velx(i) - adj_velx(i-1))/(s(i) - s(i-1));

  else

    dau(i) = (adj_velx(i+1) - adj_velx(i-1))/(s(i+1) - s(i-1));
    dav(i) = (adj_velx(i+1) - adj_velx(i-1))/(s(i+1) - s(i-1));

  end

  RHS(i) = RHS(i) + tau_2x(i)*dau(i) + tau_2y(i)*dav(i);

end

der_adj_u = dau';
der_adj_v = dav';

%end

% ********************* Computing the adjoint gradient ************************

grad = 0.0;

for i=1:NSG

    adj_vel_i = (adj_velx(i)*vnx(i)+adj_vely(i)*vny(i));
    adj_vel_ip1 = (adj_velx(i+1)*vnx(i+1)+adj_vely(i+1)*vny(i+1));
    adj_vel_avg(i) = 0.5*(adj_vel_i + adj_vel_ip1);

    crvm_avg(i) = 0.5*(crvm(i) + crvm(i+1));
 
    DX = XI(i+1)-XI(i);
    DY = YI(i+1)-YI(i);
    dls = sqrt(DX^2 + DY^2);

    grad = grad - (1.0/capl^2)*0.5*( (crvm(i)*(adj_vel_i)) ...
                       +   (crvm(i+1)*(adj_vel_ip1)) )*dls;

end

% steepest descent method with constant step size
grad
alpha=2.5E-7;
%alpha=4E-8;


% Armijo's rule
%alpha = 0.01
%sigma = 0.001
%beta = 0.25
%
%RHS = CF + sigma*alpha*grad*(-grad);
%LHS = 

capl = capl - alpha*grad

%pause

%=========
end
%=========
