function [Gx,Gy,TM] = drop_ax_adjoint_blocks ...
(NSG,ds,alpha,beta,gamma_x,gamma_y,tau_1x,tau_1y,tau_2x,tau_2y,psi_x,psi_y,s)

% ******* Initializing Gx, Gy and TM matrices *******
Gx(1:NSG+1,1:NSG+1) = 0.0;
Gy(1:NSG+1,1:NSG+1) = 0.0;
TM(1:NSG+1,1:NSG+1) = 0.0;


% ******* Computing P and Q matrices *******

for  i=1:NSG+1

  P(i) = gamma_x(i) - tau_1x(i)  - psi_x(i);
  Q(i) = gamma_y(i) - tau_1y(i)  - psi_y(i);

end


% ******* constructing TM matrix ******

for i = 2:NSG

 for j = 1:NSG+1 

  if (j == i)
 
   TM(i,j) = beta(i);

  elseif (j == i-1)

   TM(i,j) = -alpha(i)/ds(i);

  elseif (j == i+1)

   TM(i,j) = alpha(i)/ds(i);

  else

   TM(i,j) = 0.0;

  end

 end

end

%TM(1,1) = -alpha(1)/ds(1) + beta(1);
%TM(1,2) = alpha(1)/ds(1);
%
%TM(NSG+1,NSG) = -alpha(NSG+1)/ds(NSG+1);
%TM(NSG+1,NSG+1) = alpha(NSG+1)/ds(NSG+1) + beta(NSG+1); 
%

TM(1,1) = beta(1);
TM(1,2) = 0.0;

TM(NSG+1,NSG) = 0.0;
TM(NSG+1,NSG+1) = beta(NSG+1);

% ******* constructing Gx matrix *******

for i = 2:NSG

 for j = 1:NSG+1 

  if (j == i)
 
   Gx(i,j) = P(i);

  elseif (j == i-1)

   Gx(i,j) = tau_2x(i)/ds(i);

  elseif (j == i+1)

   Gx(i,j) = -tau_2x(i)/ds(i);

  else

   Gx(i,j) = 0.0;

  end

 end

end

%Gx(1,1) = P(1) + tau_2x(1)/ds(1);
%Gx(1,2) = -tau_2x(1)/ds(1);
%
%Gx(NSG+1,NSG) = tau_2x(NSG+1)/ds(NSG+1);
%Gx(NSG+1,NSG+1) = P(NSG+1) - tau_2x(NSG+1)/ds(NSG+1); 
%
Gx(1,1) = P(1);
Gx(1,2) = 0.0;

Gx(NSG+1,NSG) = 0.0;
Gx(NSG+1,NSG+1) = P(NSG+1); 




% ******* constructing Gy matrix *******

for i = 2:NSG

 for j = 1:NSG+1 

  if (j == i)
 
   Gy(i,j) = Q(i);

  elseif (j == i-1)

   Gy(i,j) = tau_2y(i)/ds(i);

  elseif (j == i+1)

   Gy(i,j) = -tau_2y(i)/ds(i);

  else

   Gy(i,j) = 0.0;

  end

 end

end

%Gy(1,1) = Q(1) + tau_2y(1)/ds(1);
%Gy(1,2) = -tau_2y(1)/ds(1);
%
%Gy(NSG+1,NSG) = tau_2y(NSG+1)/ds(NSG+1);
%Gy(NSG+1,NSG+1) = Q(NSG+1) - tau_2y(NSG+1)/ds(NSG+1); 
%

Gy(1,1) = Q(1) ;
Gy(1,2) = -tau_2y(1)/ds(1);

Gy(NSG+1,NSG) = -tau_2y(NSG+1)/s(NSG);
Gy(NSG+1,NSG+1) = Q(NSG+1) ; 



end




