function [Qx,Qy ...
         ,Wx,Wy] = pendant_slp_spline_dlp  ...
...
  (X0,Y0 ...
  ,X1,Y1...
  ,X2,Y2...
  ,NGL ...
  ,Xint1,Xint2 ...
  ,Axint,Bxint,Cxint ...
  ,Ayint,Byint,Cyint ...
  ,Dfn1,Dfn2 ...
  ,vnx1,vnx2 ...
  ,vny1,vny2 ...
  ,Dfn0 ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising ...
  )

%----------------------------------------
% Integrate the Green's function over
% a spline element
%
% Only for constant surface tension
%----------------------------------------

Iopt=1;

[ZZ,WW]=gauss_leg(NGL);

%-----------
% initialize
%-----------

Qx = 0.0D0;
Qy = 0.0D0;

Wx = 0.0D0;
Wy = 0.0D0;

%---
% prepare for the quadrature
%---

XintM = 0.5D0*(Xint2+Xint1);
XintD = 0.5D0*(Xint2-Xint1);

DfnM = 0.5D0*(Dfn2+Dfn1);
DfnD = 0.5D0*(Dfn2-Dfn1);

vnxM = 0.5D0*(vnx2+vnx1);
vnxD = 0.5D0*(vnx2-vnx1);

vnyM = 0.5D0*(vny2+vny1);
vnyD = 0.5D0*(vny2-vny1);

%--------------------------
% loop over Gaussian points
%--------------------------

for i=1:NGL

  Xint = XintM + XintD*ZZ(i);
  DX = Xint-Xint1;
  X = ((Axint*DX + Bxint )*DX + Cxint)*DX+X1;
  Y = ((Ayint*DX + Byint )*DX + Cyint)*DX+Y1;

    u(i) = velxsave + ((velxp1save - velxsave)/(X2-X1))*(X - X1);
%  if (Y1 ~= Y2) 
    v(i) = velysave + ((velyp1save - velysave)/(X2-X1))*(X - X1);
   %v(i) = cf_y(1)*Y + cf_y(2);
%  else
%   v(i) = 0.5*(velysave+velyp1save);
%  end

  
  
  
  if(Iflow==1)

   [Wxx,Wxs,Wsx,Wss ...
 ...
   ,QXXX,QXXY,QXYX,QXYY ...
   ,QYXX,QYXY,QYYX,QYYY ...
 ...
   ,PXX,PXY,PYX,PYY ...
   ,Iaxis] ...
 ...
   = sgf_ax_fs (Iopt,X0,Y0,X,Y);

  elseif(Iflow==2)

   [Wxx,Wxs,Wsx,Wss ...
   ,QXXX,QXXY,QXYX,QXYY ...
   ,QYXX,QYXY,QYYX,QYYY ...
   ,PXX,PXY,PYX,PYY ...
   ,Iaxis] ...
  ...
    = sgf_ax_w (Iopt,X0,Y0,X,Y,wall);

  elseif(Iflow==3)

   [Wxx,Wxs,Wsx,Wss]  ...
...
   = sgf_ax_1p_ct  ...
...
  (X0,Y0     ...
  ,X,Y   ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  );

  end

%---
% carry out the quadrature
%---

  Dxp = (3.0D0*Axint*DX + 2.0D0*Bxint )*DX + Cxint;
  Dyp = (3.0D0*Ayint*DX + 2.0D0*Byint )*DX + Cyint;
  h = sqrt(Dxp*Dxp+Dyp*Dyp);

  WI = h*WW(i)*XintD;

  QXXX_v(i) = QXXX*WI;
QXXY_v(i) = QXXY*WI;
QYXX_v(i) = QYXX*WI;
QYXY_v(i) = QYXY*WI;
QXYX_v(i) = QXYX*WI;
QXYY_v(i) = QXYY*WI;
QYYX_v(i) = QYYX*WI;
QYYY_v(i) = QYYY*WI;

PXX_v(i) = PXX*WI;
PXY_v(i) = PXY*WI;
PYX_v(i) = PYX*WI;
PYY_v(i) = PYY*WI;
  
  
  
  
  vnx  =  vnxM +  vnxD*ZZ(i);
  vny  =  vnyM +  vnyD*ZZ(i);
  norm = sqrt(vnx*vnx+vny*vny);
  vnx=vnx/norm;
  vny=vny/norm;

  Qx = Qx + (Wxx*vnx+Wxs*vny)*WI;
  Qy = Qy + (Wsx*vnx+Wss*vny)*WI;

  Dfn  =  DfnM + DfnD*ZZ(i);

  if(Ising==1)
    Dfn=Dfn-Dfn0;
  end

  Wx = Wx + (Wxx*vnx+Wxs*vny)*Dfn*WI;
  Wy = Wy + (Wsx*vnx+Wss*vny)*Dfn*WI;

%---
end
%---

for i=1:NGL

 QXXX_v(i) = QXXX_v(i)*DR;
 QXXY_v(i) = QXXY_v(i)*DR;
 QYXX_v(i) = QYXX_v(i)*DR;
 QYXY_v(i) = QYXY_v(i)*DR;
 QXYX_v(i) = QXYX_v(i)*DR;
 QXYY_v(i) = QXYY_v(i)*DR;
 QYYX_v(i) = QYYX_v(i)*DR;
 QYYY_v(i) = QYYY_v(i)*DR;

 PXX_v(i) = QXYX_v(i)*DR;
 PXY_v(i) = QXYY_v(i)*DR;
 PYX_v(i) = QYYX_v(i)*DR;
 PYY_v(i) = QYYY_v(i)*DR;

end




Cx = 0.0;
Cy = 0.0;

%velx0save
%u(1)
%QXXX_v(1)
%PXX_v(1)
%
%NGL
%size(QXXX_v)
%size(u)
%size(PXX_v)
%size(vnx)

vnxM = 0.5D0*(vnx2+vnx1);
vnxD = 0.5D0*(vnx2-vnx1);

vnyM = 0.5D0*(vny2+vny1);
vnyD = 0.5D0*(vny2-vny1);


for i = 1:NGL
    
  vnx  =  vnxM +  vnxD*ZZ(i);
  vny  =  vnyM +  vnyD*ZZ(i);
  norm = sqrt(vnx*vnx+vny*vny);
  vnx=vnx/norm;
  vny=vny/norm;    
    
    
   Cx = Cx + (QXXX_v(i)*(u(i) - velx0save) + QXYX_v(i)*v(i) - PXX_v(i)*vely0save)*vnx ...
	   + (QXXY_v(i)*(u(i) - velx0save) + QXYY_v(i)*v(i) - PXY_v(i)*vely0save)*vny;
   Cy = Cy + (QYXX_v(i)*(u(i) - velx0save) + QYYX_v(i)*v(i) - PYX_v(i)*vely0save)*vnx ...
	   + (QYXY_v(i)*(u(i) - velx0save) + QYYY_v(i)*v(i) - PYY_v(i)*vely0save)*vny;
end




%-----
% done
%-----

return
