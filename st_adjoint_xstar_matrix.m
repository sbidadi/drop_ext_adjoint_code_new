function [F, G, H] = st_adjoint_xstar_matrix(n, kappa, u, Ca)
%function [F, G, H] = st_adjoint_xstar_matrix(n, kappa, J, u, Ca)

    % ST_ADJOINT_XSTAR_MATRIX Compute matrices in xstar equation.
    %
    % The equation is defined as:
    %
    %   F x^* + G u^* + H v^* = b
    %
    % and this function computes the `F`, `G` and `H` matrices.
    %
    % INPUTS:
    %   N:          normal vector components.
    %   KAPPA:      mean curvature (sum of principal curvatures).
    %   J:          metric term.
    %   U:          velocity components (not adjoint velocity!).
    %   CA:         capillary number.
    %
    % OUTPUTS:
    %   F, G, H:    matrix operators for each term in the equation.

    % number of points
    npoints = length(kappa);
    % uniform grid size
    dxi = 0.5 / (npoints - 1);
    % inverse capillary number
    Cai = 1.0 / Ca;

    % compute metric term averages
%    Ji = 1.0 ./ J;
%    Jim = 0.5 * (Ji(1:end - 1) + Ji(2:end));

    Ji = eye(npoints);
    Jim = eye(npoints);

    % compute tangential velocity
    ut = dot(u, [-n(2, :); n(1, :)]);

    % compute F matrix
    F = zeros(npoints);
    for i = 2:npoints - 1
        F(i, i + 1) =  Ji(i) * ut(i) / (2.0 * dxi);
        F(i, i + 0) =  Ji(i) * (ut(i + 1) - ut(i - 1)) / (2.0 * dxi);
        F(i, i - 1) = -Ji(i) * ut(i) / (2.0 * dxi);
    end

    F(1, 1) = Ji(1) * ut(2) / dxi;
    F(end, end) = -Ji(end) * ut(end - 1) / dxi;

    % compute Gx matrix
    G = zeros(npoints);
    for i = 2:npoints - 1
        G(i, i + 1) =  Cai * Ji(i) * (kappa(i + 1) * n(2, i + 1) / (2.0 * dxi) + ...
                                      Jim(i + 0) * n(1, i + 1) / dxi^2);
        G(i, i + 0) =  Cai * n(1, i) * (kappa(i)^2 - ...
                                        Ji(i) * (Jim(i) + Jim(i - 1)) / dxi^2);
        G(i, i - 1) = -Cai * Ji(i) * (kappa(i - 1) * n(2, i - 1) / (2.0 * dxi) - ...
                                      Jim(i - 1) * n(1, i - 1) / dxi^2);
    end

    % NOTE: hardcoding nx = 1 at the boundary
    G(1, 1) = Cai * (kappa(1)^2 - 2.0 * Ji(1) * Jim(1) / dxi^2);
    G(1, 2) = 2.0 * Cai * Ji(1) * (kappa(2) * n(2, 2) / (2.0 * dxi) + ...
                                   Jim(1) * n(1, 2) / dxi^2);
    G(end, end - 1) = -2.0 * Cai * Ji(end) * (kappa(end - 1) * n(2, end - 1) / (2.0 * dxi) - ...
                                              Jim(end) * n(1, end - 1) / dxi^2);
    G(end, end) = -Cai * (kappa(end)^2 - 2.0 * Ji(end) * Jim(end) / dxi^2);

    % compute H matrix
    H = zeros(npoints);
    for i = 2:npoints - 1
        H(i, i + 1) = -Cai * Ji(i) * (kappa(i + 1) * n(1, i + 1) / (2.0 * dxi) - ...
                                      Jim(i + 0) * n(2, i + 1) / dxi^2);
        H(i, i + 0) =  Cai * n(2, i) * (kappa(i)^2 - ...
                                        Ji(i) * (Jim(i) + Jim(i - 1)) / dxi^2);
        H(i, i - 1) =  Cai * Ji(i) * (kappa(i - 1) * n(1, i - 1) / (2.0 * dxi) + ...
                                      Jim(i - 1) * n(2, i - 1) / dxi^2);
    end

    % NOTE: hardcoding ny = 0 at the boundary
    H(1, 2) = 2.0 * Cai * Ji(1) * (kappa(2) * n(1, 2) / (2.0 * dxi)+ ...
                                           Jim(1)*n(2,2)/dxi^2);
    H(end, end - 1) = -2.0 * Cai * Ji(end) * (kappa(end - 1) * n(1, end - 1) / (2.0 * dxi) - ...
                                        Jim(end)*n(2,end-1)/dxi^2);
end
