function [Px,Ps] = pendant_dlp_line_pressure ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL)

%----------------------------------------
% integrate the Green's function over
% a straight segment
%----------------------------------------

Iopt=1;

[ZZ,WW]=gauss_leg(NGL);

%-----------
% initialize
%-----------

Px = 0.0D0;
Ps = 0.0D0;

%---
% prepare for the quadrature
%---

XM = 0.5D0*(X2+X1);
XD = 0.5D0*(X2-X1);
YM = 0.5D0*(Y2+Y1);
YD = 0.5D0*(Y2-Y1);
DR = sqrt(XD*XD+YD*YD);

%--------------------------
% loop over Gaussian points
%--------------------------

for i=1:NGL

  X = XM+XD*ZZ(i);
  Y = YM+YD*ZZ(i);

  [Gx,Gs] = lgf_ax_fs (X,Y,X0,Y0);

%---
% carry out the quadrature
%---

WI = WW(i);

Px = Px + Gx * Y * WI;
Ps = Ps + Gs * Y * WI;

end

%---
% finish up
%---

Px = Px*DR;
Ps = Ps*DR;

%-----
% done
%-----

return
