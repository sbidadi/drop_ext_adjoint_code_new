%---
clear all
close all
clc
%---

format long g
check = 1;

%===========================
% motion of a viscous drop
%===========================
%Iflow = 3;   % inside a cylinder
Iflow = 1;   % free space
%Iflow = 2;   % above a wall
%---

lambda = 1; % viscosity ratio
a = 1.0;   % drop radius

wall = 0.0;

if(Iflow==1)
   xcnt = 0.0;
elseif(Iflow==2)
   xcnt = 2.0;
elseif(Iflow==3)
   xcnt = 0.0;
end

sc = 1.2;
RL = 4.0;
Nsum = 2;
Np = 2;

%---
% prepare
%---

VD = 4.0*pi*a^3/3;   % drop volume
  
% ---------------------------------
% Guess an initial capillary number
capl = 0.04+(10^-6);
% ---------------------------------

grad = 1.0;
eps = 0.00001;
K = 1;

%while abs(grad) > eps

%fileID = fopen('FDS_CF_Capl_lambda1_right_0.000000001.txt','w');
fileID = fopen('FDS_CF_Capl_lambda1_right_0.0001_N_30.txt','w');

%while capl <= 0.059

%---
% interface perturbation amplitude
%---

%  amp=-0.2*a;
%  amp= 0.0*a;
%  amp= 0.2*a;
amp = 0.0;

%--------
% interfacial segments
%-------

 NSG=08;
 NSG=256;
 NSG=128;
 NSG=64;
 NSG=96;
 NSG=100;
 NSG=14;
 NSG=80;
 NSG=40;
 NSG=100;
 NSG=60;
 NSG=40;
% NSG = 80;
% NSG=50; 
%NSG=70;
%NSG=100;

%--------
% Gauss-Legendre quadrature
%-------

% NGL=2;
% NGL=12;
 NGL=6;

%---
% time stepping
%---

%IRK = 2; % RK2
IRK = 1; % Euler

% Dt=5.0; 
% Dt=2.5;
% Dt=1.0;
% Dt=2.5;
% Dt=0.075;
% Dt=0.050;
% Dt=0.05;
% Dt=0.020;
% Dt=0.1;
Dt=0.001;

move=1; % normal velocity
%move=0; % total velocity

%Nstep=1600;
Nstep=40;
Nstep=1;
Nstep=10;
Nstep=30;
Nstep=50;
Nstep=5;
Nstep=20;
Nstep=60;
Nstep=180;
Nstep=3600;
Nstep=2400;
Nstep=1200;
Nstep=600;
Nstep=5000;
Nstep=10;
Nstep = 500000;
%Nstep = 2;

%---
% integration over the interface
%---

 Ichoose=1; % lines
 %Ichoose=2; % splines

%---
% point redistribution
%---

Ich1=0;
%Ich1=1;
%thmax=0.25*pi;
%thmax=0.0015*pi;
thmax=0.125*pi;
thmax=(pi/180)*5;

Ich2=0;
Ich2=1;
spmax=pi*a/NSG;
%spmax=0.01*pi/NSG;
%spmax=0.5*pi*a/NSG;
spmax = 0.01
%spmax = 0.0001

Ich3=0;
%Ich3=1; 
spmin=spmax/5.0;
%spmin=spmax/2.0;
spmin=0.05

%-------------
% initial shape
%-------------

   for i=1:NSG+1
    angle = (i-1.0)*(pi/NSG);
    %rad = a + amp*cos(2*angle);
    XI(i) = cos(angle);
    YI(i) = sin(angle);
   end
  
   dXI = min(abs(diff(XI)));
   dYI = min(abs(diff(YI)));
   dl = sqrt(dXI^2 + dYI^2);
   
 % Increasing the number of points at the ends of the drop
 [NSG,XI,YI ...
 ,vnx,vny,crv,s ...
 ,Xint ...
 ,Axint,Bxint,Cxint ...
 ,Ayint,Byint,Cyint ...
 ,vlm ...
 ,Istop,Ido,action] ...
 ...
 = prd_2d (NSG,XI,YI,Ich1,thmax,Ich2,spmax,Ich3,spmin);  

%-------------
% plotting
%-------------

% figure(1)
% hold on
% axis equal
% %  plot( YI,-XI,'-k.')
% %  plot(-YI,-XI,'r:')
% 
% plot( XI,YI,'-k.')
% plot( -XI,YI,'r:')
% %plot(-YI,-XI,'r:')
% box on

%-----------
% initialize
%-----------

time(1)=0.0;
terminal =0.0;

%================
for step=1:Nstep
%================

%---
% animation
%---

clear XXI YYI

for i=1:NSG+1
  XXI(i)= XI(i);
  YYI(i)= YI(i);
%   if(Iflow==1)
%     XXI(i) = XXI(i)+terminal*time(step);
%   end
end

for i=1:NSG
  XXI(NSG+1+i) = XI(NSG+1-i);
  YYI(NSG+1+i) = -YI(NSG+1-i);
%   if(Iflow==1)
%     XXI(NSG+1+i) = XXI(NSG+1+i)+terminal*time(step);
%   end
end

if(step==1)
  figure(2)
   Handle1 = patch( XXI, YYI, 'y');
   hold on
   Handle2 = plot( XXI, YYI, '.-');
  
  axis equal
  set(gca,'fontsize',12)
  xlabel('z','fontsize',12)
  ylabel('r','fontsize',12)
%  axis([-1.2  1.2 -1.6 1.6])
   axis([-5 5 -3 3])
   if(Iflow==2) 
      patch([-2.0 2.0 2.0 -2.0],[0.0 0.0 -0.1 -0.2],'r')
      axis([-2.0 2.0 -0.1 4])
   end
   if(Iflow==3)
      plot( YYI, XXI+RL, '.-')
      plot( YYI, XXI-RL, '.-')
      plot( YYI, XXI-2*RL, '.-')
   end
  box on
else
set(Handle1,'XData',[XXI] ...
           ,'YData',[YYI]);
set(Handle2,'XData',[XXI] ...
             ,'YData',[YYI]);

  drawnow

end


%---
% point redistribution
%---

Ido=1;
Iloop=0;

% while(Ido==1)
% % %--
% % 
%    Iloop=Iloop+1;
%  
% %   if (mod(step,10) == 1)
%       
%   [NSG,XI,YI ...
%   ,vnx,vny,crv,s ...
%   ,Xint ...
%   ,Axint,Bxint,Cxint ...
%   ,Ayint,Byint,Cyint ...
%   ,vlm ...
%   ,Istop,Ido,action] ...
%   ...
%   = prd_2d (NSG,XI,YI,Ich1,thmax,Ich2,spmax,Ich3,spmin);
% 
% %   else
% %       Ido = 0;
% %   end
%   
% % 
% % %   else
% % %       
% % %   [NSG,XI,YI ...
% % %   ,velxsave,velysave,vnx,vny,crv,s ...
% % %   ,Xint ...
% % %   ,Axint,Bxint,Cxint ...
% % %   ,Ayint,Byint,Cyint ...
% % %   ,vlm ...
% % %   ,Istop,Ido,action] ...
% % %   ...
% % %   = prd_2d_vel (NSG,XI,YI,velxsave,velysave,Ich1,thmax,Ich2,spmax,Ich3,spmin);
% % % 
% % %   end
% % %   
% % %   if(Istop==1) 
% % % 
% % % 	break; 
% % %         
% % %    end
% % % % 
% % 
% end
% % %--
% %  
%   if(Istop==1)
%     break; 
%   end

 for i=1:NSG+1
  XIT(step,i) = XI(i);
  YIT(step,i) = YI(i);
 end

 NSEG(step)=NSG;

%---
% drop volume
%---

vlm=0.0;

for i=1:NSG
 DX = XI(i+1)-XI(i);
 vlm=vlm + YI(i)*YI(i)*DX;
end

vlm=-vlm/VD

volume(step)=vlm;

[vnx,vny,crv,s ...
,Xint ...
,Axint,Bxint,Cxint ...
,Ayint,Byint,Cyint ...
,vlm] = splc_geo (NSG,XI,YI);

%---
% mean curvature
%---

crvm(1)     = -crv(1);
%crvm(NSG+1) = -crv(1);
crvm(NSG+1) = -crv(NSG+1);

for i=2:NSG
 %crvm(i)=-0.5*(crv(i)-vny(i)/YI(i));
 crvm(i)=(-crv(i)+vny(i)/abs(YI(i)));
 %crvm(i) = -crv(i);
end

%---
% jump in normal traction
%---

for i=1:NSG+1
  Dfn(i) = (1.0/capl)*crvm(i);
end

%---
% plot
%---

Iskip=0;
Iskip=1;

if(Iskip==0)
 figure(3)
 plot(s,crv)
 hold on
 plot(s,vnx)
 plot(s,vny)
 plot(s,crvm,'o-')
 plot(s,Dfn,'r')
end

%==============================
%---
% Computing the velocities at the marker points
%---

% Coefficients associated with the free stream, 
% single-layer potential and double-layer potential 
cf_1 = 2.0/(1+lambda);
cf_2 = -1.0/(4*pi*(1+lambda));
cf_3 = (1-lambda)/(4*pi*(1+lambda));

FI = NSG;
%FI = NSG/2;

% Computing the coefficients for cubic interpolation
if (step > 1)
  [ax,bx,cx] = splc_clm(FI,XI,velxsave,0.0,0.0);
  [ay,by,cy] = splc_clm(FI,XI,velysave,0.0,0.0);
end

%---
 for j=1:FI+1  % run over nodes
%---

 X0   =  XI(j);
 Y0   =  YI(j);
 Dfn0 = Dfn(j);

 sum1 = 0.0;  % to test identities
 sum2 = 0.0;

 vx_slp = 0.0;
 vy_slp = 0.0;

 vx_dlp = 0.0;
 vy_dlp = 0.0;

 Cx_dlp = 0.0;
 Cy_dlp = 0.0;
 
% velx0save = velxsave(j);
% vely0save = velysave(j);
 
%---
for i=1:FI % run over segments
%---

 X1 = XI(i);
 Y1 = YI(i);
 X2 = XI(i+1);
 Y2 = YI(i+1);
 
 if (i > 1)
     X3 = XI(i-1);
     Y3 = YI(i-1);
 end

 Ising=0;

%---
 if(Ichoose==1)  % lines
%---

if(j==i | j==i+1)
  Ising=1;
end

 % ---- computing the single-layer potential vx_slp, vy_slp ---
 % computing the Green's function 
 [Qxx,Qxs ...
 ,Qsx,Qss] = drop_ax_slp_line ...
 ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising);

  vnx0=0.5*(vnx(i)+vnx(i+1));
  vny0=0.5*(vny(i)+vny(i+1));
  vnx0=-YI(i+1)+YI(i);
  vny0= XI(i+1)-XI(i);
  norm = sqrt(vnx0^2+vny0^2);
  vnx0 = vnx0/norm;
  vny0 = vny0/norm;

  sum1 = sum1 + Qxx*vnx0+Qxs*vny0;
  sum2 = sum2 + Qsx*vnx0+Qss*vny0;

  Dfx0=0.5*(Dfn(i)*vnx(i)+Dfn(i+1)*vnx(i+1));
  Dfy0=0.5*(Dfn(i)*vny(i)+Dfn(i+1)*vny(i+1));

  vx_slp = vx_slp + Qxx*Dfx0 + Qxs*Dfy0;
  vy_slp = vy_slp + Qsx*Dfx0 + Qss*Dfy0;

% ---- computing the double-layer potential vx_dlp, vy_dlp ----
  if (step > 1)
      
   vnx_seg(i)=0.5*(vnx(i)+vnx(i+1));
   vny_seg(i)=0.5*(vny(i)+vny(i+1));

   vnx1  = vnx(i); vnx2 = vnx(i+1);
   vny1  = vny(i); vny2 = vny(i+1);
  
  if (i == 1)
    
%       **** linear interpolation ****
      [Cx,Cy] = drop_ax_slp_line_dlp  ...
      ...
      (X0,Y0 ...
      ,X1,Y1...
      ,X2,Y2...
      ,NGL ...
      ,Iflow ...
      ,wall ...
      ,sc      ...
      ,RL      ...
      ,Nsum,Np ...
      ,Ising ...
      ,velxsave(i) ...
      ,velysave(i) ...
      ,velxsave(i+1) ...
      ,velysave(i+1) ...
      ,velxsave(j) ...
      ,velysave(j) ...
      ,vnx_seg(i) ...
      ,vny_seg(i));

%   [Cx,Cy] = drop_ax_slp_line_dlp  ...
%   ...
%   (X0,Y0 ...
%   ,X1,Y1...
%   ,X2,Y2...
%   ,NGL ...
%   ,Iflow ...
%   ,wall ...
%   ,sc      ...
%   ,RL      ...
%   ,Nsum,Np ...
%   ,Ising ...
%   ,velxsave(i) ...
%   ,velysave(i) ...
%   ,velxsave(i+1) ...
%   ,velysave(i+1) ...
%   ,velxsave(j) ...
%   ,velysave(j) ...
%   ,vnx1,vnx2 ...
%   ,vny1,vny2);


  else
      
     % **** quadratic interpolation ****
     [Cx,Cy] = drop_ax_slp_line_dlp_2  ...
      ...
      (X0,Y0 ...
      ,X1,Y1...
      ,X2,Y2...
      ,X3,Y3...
      ,NGL ...
      ,Iflow ...
      ,wall ...
      ,sc      ...
      ,RL      ...
      ,Nsum,Np ...
      ,Ising ...
      ,velxsave(i) ...
      ,velysave(i) ...
      ,velxsave(i-1) ...
      ,velysave(i-1) ...
      ,velxsave(i+1) ...
      ,velysave(i+1) ...
      ,velxsave(j) ...
      ,velysave(j) ...
      ,vnx_seg(i) ...
      ,vny_seg(i));    
      
   end  

%     [Cx,Cy] = drop_ax_slp_line_dlp_3  ...
%         ...
%         (X0,Y0 ...
%         ,X1,Y1...
%         ,X2,Y2...
%         ,NGL ...
%         ,Iflow ...
%         ,wall ...
%         ,sc ...
%         ,RL ...
%         ,Nsum,Np ...
%         ,Ising ...
%         ,ax(i),bx(i),cx(i) ...
%         ,ay(i),by(i),cy(i) ...
%         ,velxsave(i),velysave(i) ...
%         ,velxsave(j) ...
%         ,velysave(j) ...
%         ,vnx_seg(i) ...
%         ,vny_seg(i));

    Cx_dlp = Cx_dlp + Cx;
    Cy_dlp = Cy_dlp + Cy;
    
   else
 
    vx_dlp = 0.0;
    vy_dlp = 0.0;

  end

%---
 else  % spline
%---

  Ising =1;

  Ax=Axint(i); Bx=Bxint(i); Cx=Cxint(i);
  Ay=Ayint(i); By=Byint(i); Cy=Cyint(i);

  Xint1 = Xint(i); Xint2 =Xint(i+1);
  Dfn1  =  Dfn(i); Dfn2  = Dfn(i+1);
  vnx1  =  vnx(i); vnx2   =vnx(i+1);
  vny1  =  vny(i); vny2   =vny(i+1);

  [Qx,Qy ...
  ,Wx,Wy] = drop_ax_slp_spline ...
  ...
  (X0,Y0 ...
  ,X1,Y1 ...
  ,X2,Y2 ...
  ,NGL ...
  ,Xint1,Xint2 ...
  ,Ax,Bx,Cx ...
  ,Ay,By,Cy ...
  ,Dfn1,Dfn2 ...
  ,vnx1,vnx2 ...
  ,vny1,vny2 ...
  ,Dfn0 ...
  ,Iflow ...
  ,wall ...
  ,sc      ...
  ,RL      ...
  ,Nsum,Np ...
  ,Ising ...
  );

  sum1 = sum1 + Qx;
  sum2 = sum2 + Qy;

  vx_slp = vx_slp + Wx;
  vy_slp = vy_slp + Wy;

%---
 end
%---

%---
 end % of interfacial segments
%---

% free-stream velocity at each nodal (pole) point  
 velx_inf = XI(j);
 vely_inf = -YI(j)/2.0;

% net velocity at each pole point
 if (step > 1)
   vx_dlp = Cx_dlp - 4*pi*velxsave(j);
   vy_dlp = Cy_dlp - 4*pi*velysave(j);
  
% vx_dlp = Bx_dlp;
% vy_dlp = Bx_dlp;
  
 end
 
 velx(j) = cf_1*velx_inf + cf_2*vx_slp + cf_3*vx_dlp;
 vely(j) = cf_1*vely_inf + cf_2*vy_slp + cf_3*vy_dlp;

%  velx(j) = cf_1*velx_inf + cf_2*vx_slp;
%  vely(j) = cf_1*vely_inf + cf_2*vy_slp;
 
%---
 end % of nodes
%---

%============================

 for i=1:NSG+1
   veln(i) = vnx(i)*velx(i)+vny(i)*vely(i);
 end

 velxsave=velx;
 velysave=vely;
 velnsave=veln;
 vnxsave=vnx;
 vnysave=vny;
 
 if (mod(NSG,2) == 0)

  Half = NSG/2;
  crvm_half = crvm(1:Half+1);
  

  for i=1:Half+1
      
   if(move==0)
     XI(i) = XI(i)+Dt*velx(i);
     YI(i) = YI(i)+Dt*vely(i);
   else
     XI(i) = XI(i)+Dt*veln(i)*vnx(i);
     YI(i) = YI(i)+Dt*veln(i)*vny(i);
     veln_new(i) = veln(i);

   end
  end


  for i=1:Half
    XI(Half+1+i) = -XI(Half+1-i);
    YI(Half+1+i) = YI(Half+1-i);
  end
  
 else
  
  Half = round(NSG/2)
  
  for i=1:Half
   if(move==0)
     XI(i) = XI(i)+Dt*velx(i);
     YI(i) = YI(i)+Dt*vely(i);
   else
     XI(i) = XI(i)+Dt*veln(i)*vnx(i);
     YI(i) = YI(i)+Dt*veln(i)*vny(i);
     veln_new(i) = veln(i);     
         
   end
  end
  
 
  for i=1:Half
    XI(Half+i) = -XI(Half+1-i);
    YI(Half+i) = YI(Half+1-i);

  end
  
 end
 
%---
% one more step
%---
if (all(abs(veln_new(:)) < 0.1) == 1)

  
  if (mod(NSG,2) == 0)

    Half = NSG/2;
    
    for i=1:Half
      velx(Half+1+i) = -velx(Half+1-i);
      vely(Half+1+i) = vely(Half+1-i);
    end
  
  else

    Half = round(NSG/2);

    for i=1:Half
      velx(Half+i) = -velx(Half+1-i);
      vely(Half+i) = vely(Half+1-i);
    end
 

  end

%  fprintf('going inside the file')
%  fileID = fopen('pressure_Capl_0p06_lamb_10.txt','w');
  for j=1:NSG+1
    P(j) = (1/capl)*crvm(j);
%    fprintf(fileID,'%20.14f\t%20.14f\t%20.14f%20.14f\n',XI(j),YI(j),velx(j),vely(j));
%    fprintf(fileID,'%2.5f %2.5f %2.5f\n',XI(j),YI(j),P(j));
  end
%  fclose(fileID);  
 break;

end

dll = min(s);
time(step+1)=time(step)+Dt;

step
NSG

clear velx vely

%=========
end
%=========
%---
% save the last profile
%---

 for i=1:NSG+1
  XIT(Nstep+1,i) = XI(i);
  YIT(Nstep+1,i) = YI(i);
 end
 NSEG(Nstep+1)=NSG;

%---
% plot
%---

Ido =0;

if(Ido==1)

figure(99)
hold on
axis equal

many=Nstep+1;

for step=1:many
 if(step==1)
  ntp = NSEG(step)+1;
  plot(XIT(step,1:ntp), YIT(step,1:ntp),'r.-')
  plot(XIT(step,1:ntp),-YIT(step,1:ntp),'r.-')
 elseif(step==many)
  ntp = NSEG(step)+1;
  plot(XIT(step,1:ntp), YIT(step,1:ntp),'c.-')
  plot(XIT(step,1:ntp),-YIT(step,1:ntp),'c.-')
 else
  ntp = NSEG(step)+1;
  plot(XIT(step,1:ntp), YIT(step,1:ntp),'b.-')
  plot(XIT(step,1:ntp),-YIT(step,1:ntp),'b.-')
 end
end

end

% ---------------
% cost functional
% ---------------
 velx
 vely


 Pfile = dlmread('pressure_Capl_0p06_lamb_1_N_40_udotn_0p1_v3.txt','\t');
 velx_ref = Pfile(:,3);
 vely_ref = Pfile(:,4);
 P_ref = Pfile(:,5);
 
 CF = 0.0;

 for i = 1:NSG

   P_avg(i) = 0.5*(P(i) + P(i+1));
   P_ref_avg(i) = 0.5*(P_ref(i) + P_ref(i+1));
   
   DX = XI(i+1)-XI(i);
   DY = YI(i+1)-YI(i);
   dl = sqrt(DX^2 + DY^2);
%   XI_diff_i = XI(i) - XI_ref(i);
%   YI_diff_i = YI(i) - YI_ref(i);

   velx_diff_i = velx(i) - velx_ref(i);
   vely_diff_i = vely(i) - vely_ref(i);
   D2_i = velx_diff_i^2 + vely_diff_i^2;

%   D2_i = XI_diff_i^2 + YI_diff_i^2;
%   XI_diff_ip1 = XI(i+1) - XI_ref(i+1);
%   YI_diff_ip1 = YI(i+1) - YI_ref(i+1);
%   D2_ip1 = XI_diff_ip1^2 + YI_diff_ip1^2;

   velx_diff_ip1 = velx(i+1) - velx_ref(i+1);
   vely_diff_ip1 = vely(i+1) - vely_ref(i+1);
   D2_ip1 = velx_diff_ip1^2 + vely_diff_ip1^2;

   CF = CF + 0.5*0.5*dl*( D2_i + D2_ip1 ); % trapezoidal rule

%   CF = CF + 0.5*0.5*dl*( D2_i + D2_ip1 ); % trapezoidal rule
%   CF2 = CF2 + 0.5*0.5*dl*( (P(i)-P_ref(i))^2 + (P(i+1)-P_ref(i+1))^2 );

 end

% CF_v_new = smooth(NSG-1,CF_v,1);
% for i = 1:NSG
%	CF = CF + CF_v_new(i)
% end

 CF

% break;


% capl_vec(K) = capl*100;
% CF_vec(K) = CF;
%
% fprintf(fileID,'%20.14f\t%20.14f\n',capl,CF);
%
% CF_vecn(K) = CF_vec(K)/CF_vec(1);
%
% figure(16)
% hold on
% axis equal
% plot(Xint,XI,'-k');
% plot(Xint,YI,'-r');
% box on
% hold off
% legend('X_{pos}', 'Y_{pos}');
% xlabel('Arc length');
% ylabel('X_{pos},Y_{pos}');
%
% fprintf('going inside drop_ax_m_adjoint....');
 
%[capl,grad] = drop_ax_m_adjoint ...
%   (NSG,XI,YI,capl,P,crvm,vnx,vny, ...
%   Iflow,wall,sc,RL,Nsum,Np);
% 
% grad_vec(K) = grad;
% 
%figure(10)
%hold on
%axis equal
%%  plot( YI,-XI,'-k.')
%%  plot(-YI,-XI,'r:')
%
%plot(capl_vec,CF_vecn,'-ok','linewidth',3)
%xlim([6 8])
%ylim([-0.1 1.1])
%%ylim([0 500])
%%plot(-YI,-XI,'r:')
%title('For Lambda = 0.1')
%xlabel('C*100')
%ylabel('J')
%box on
%
% 
 K = K + 1;


%capl = capl - 0.000000001

capl = capl - 0.0001

%capl = capl - 0.001

%end

 
