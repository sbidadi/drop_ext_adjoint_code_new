function [Gx,Gy,TM] = drop_ax_adjoint_blocks_alex ...
(NSG,ds,alpha,beta,gamma_x,gamma_y,tau_1x,tau_1y,tau_2x,tau_2y,psi_x,psi_y,inv_capl, ...
vnx,vny,vtx,vty,crvm)

% ******* Initializing Gx, Gy and TM matrices *******
Gx(1:NSG+1,1:NSG+1) = 0.0;
Gy(1:NSG+1,1:NSG+1) = 0.0;
TM(1:NSG+1,1:NSG+1) = 0.0;


% ******* Computing P and Q matrices *******

for  i=1:NSG+1

  P(i) = gamma_x(i) - tau_1x(i)  - psi_x(i);
  Q(i) = gamma_y(i) - tau_1y(i)  - psi_y(i);

end


% ******* constructing TM matrix ******

for i = 2:NSG

 for j = 1:NSG+1 

  if (j == i)
 
   TM(i,j) = beta(i);

  elseif (j == i-1)

   TM(i,j) = -alpha(i)/ds(i);

  elseif (j == i+1)

   TM(i,j) = alpha(i)/ds(i);

  else

   TM(i,j) = 0.0;

  end

 end

end

TM(1,1) = -alpha(1)/ds(1) + beta(1);
TM(1,2) = alpha(1)/ds(1);

TM(NSG+1,NSG) = -alpha(NSG+1)/ds(NSG+1);
TM(NSG+1,NSG+1) = alpha(NSG+1)/ds(NSG+1) + beta(NSG+1); 


% ******* constructing Gx matrix *******

for i = 2:NSG

 for j = 1:NSG+1 

  if (j == i)
 
   Gx(i,j) = tau_1x(i) - (2*inv_capl*vnx(i))/(ds(i)^2) + gamma_x(i);

  elseif (j == i-1)

   Gx(i,j) = (inv_capl/(ds(i)^2))*vnx(i-1) + (inv_capl/ds(i))*crvm(i-1)*vtx(i-1);

  elseif (j == i+1)

   Gx(i,j) = (inv_capl/(ds(i)^2))*vnx(i+1) - (inv_capl/(ds(i)))*crvm(i+1)*vtx(i+1);

  else

   Gx(i,j) = 0.0;

  end

 end

end

Gx(1,1) = tau_1x(1) + gamma_x(1) - (2*inv_capl*vnx(1))/(ds(1)^2) + ...
          (inv_capl*crvm(1)*vtx(1))/ds(1);
Gx(1,2) = 2.0*inv_capl*vnx(2)/(ds(1)^2) - inv_capl*crvm(2)*vtx(1)/(ds(1)^2);

Gx(NSG+1,NSG) = (inv_capl*crvm(NSG)*vtx(NSG)/(ds(NSG+1))) + (2.0*inv_capl*vnx(NSG)/(ds(NSG+1)^2));
Gx(NSG+1,NSG+1) = tau_1x(NSG+1) + gamma_x(NSG+1) - (2*inv_capl*vnx(NSG+1))/(ds(NSG+1)^2) - ...
                  (inv_capl*crvm(NSG+1)*vtx(NSG+1))/ds(NSG+1); 

%Gx(NSG+1,NSG) = 0.0;
%Gx(NSG+1,NSG+1) = 0.0;

% ******* constructing Gy matrix *******

for i = 2:NSG

 for j = 1:NSG+1

  if (j == i)
 
   Gy(i,j) = tau_1y(i) - (2*inv_capl*vny(i))/(ds(i)^2) + gamma_y(i);

  elseif (j == i-1)

   Gy(i,j) = (inv_capl/(ds(i)^2))*vny(i-1) + (inv_capl/(ds(i)))*crvm(i-1)*vty(i-1);

  elseif (j == i+1)

   Gy(i,j) = (inv_capl/(ds(i)^2))*vny(i+1) - (inv_capl/(ds(i)))*crvm(i+1)*vty(i+1);

  else

   Gy(i,j) = 0.0;

  end

 end

end

Gy(1,1) = tau_1y(1) + gamma_y(1) - (2*inv_capl*vny(1))/(ds(1)^2) + ...
          (inv_capl*crvm(1)*vty(1))/ds(1);
Gy(1,2) = 2.0*inv_capl*vny(2)/(ds(1)^2) - inv_capl*crvm(2)*vty(1)/(ds(1)^2);

Gy(NSG+1,NSG) = (inv_capl*crvm(NSG)*vty(NSG)/(ds(NSG+1))) + (2.0*inv_capl*vny(NSG)/(ds(NSG+1)^2));
Gy(NSG+1,NSG+1) = tau_1y(NSG+1) + gamma_y(NSG+1) - (2*inv_capl*vny(NSG+1))/(ds(NSG+1)^2) - ...
                  (inv_capl*crvm(NSG+1)*vty(NSG+1))/ds(NSG+1); 

end




